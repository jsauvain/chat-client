package ch.joel.client.ui;

import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.controlsfx.control.textfield.AutoCompletionBinding;

import javafx.beans.value.ChangeListener;
import javafx.scene.control.TextField;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class EditedAutoCompletionTextField extends AutoCompletionBinding<String> {
	private StringConverter<String> converter;
	private final ChangeListener<Boolean> focusChangedListener;

	private static StringConverter<String> defaultStringConverter() {
		return new StringConverter<String>() {
			public String toString(String t) {
				return t == null ? null : t;
			}

			@Override
			public String fromString(java.lang.String string) {
				return string;
			}
		};
	}
	
	public EditedAutoCompletionTextField(TextField textField, Callback<ISuggestionRequest, Collection<String>> suggestionProvider) {
		this(textField, suggestionProvider, defaultStringConverter());
	}

	public EditedAutoCompletionTextField(TextField textField, Callback<ISuggestionRequest, Collection<String>> suggestionProvider, StringConverter<String> converter) {
		super(textField, suggestionProvider, converter);
		this.focusChangedListener = (obs, oldV, newV) -> {
			if (!newV.booleanValue()) {
				EditedAutoCompletionTextField.this.hidePopup();
			}
		};
		this.converter = converter;
		this.getCompletionTarget().focusedProperty().addListener(this.focusChangedListener);
	}

	public TextField getCompletionTarget() {
		return (TextField) super.getCompletionTarget();
	}

	public void dispose() {
		this.getCompletionTarget().focusedProperty().removeListener(this.focusChangedListener);
	}

	@Override
	protected void completeUserInput(String completion) {
		String newText = this.converter.toString(completion);
		int at = getCompletionTarget().getCaretPosition();
		Matcher matcher = Pattern.compile(".*(\\(.*)").matcher(getCompletionTarget().getText().substring(0, at));
		if (matcher.matches()) {
			String toReplace = matcher.group(1);
			String finalText = getCompletionTarget().getText().replace(toReplace, newText);
			this.getCompletionTarget().setText(finalText);
		}

	}
}
