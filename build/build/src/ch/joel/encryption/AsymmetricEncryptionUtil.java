package ch.joel.encryption;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;

public class AsymmetricEncryptionUtil {

	private final String ALGORITHM = "RSA";

	public byte[] encrypt(byte[] toEncodeBytes, PublicKey serverPublicKey) {
		byte[] cipherText = null;
		try {
			final Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, serverPublicKey);
			cipherText = cipher.doFinal(toEncodeBytes);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cipherText;
	}

	public byte[] encrypt(byte[] toEncodeBytes, String publicKeyRepresentingInBase) {
		return encrypt(toEncodeBytes, getKey(publicKeyRepresentingInBase));
	}

	private PublicKey getKey(String base) {
		byte[] publicBytes = Base64.getDecoder().decode(base);
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
		KeyFactory keyFactory;
		try {
			keyFactory = KeyFactory.getInstance("RSA");
			return keyFactory.generatePublic(keySpec);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}