package ch.joel.client.ui;

import java.util.Optional;

import ch.joel.client.Client;
import ch.joel.client.ui.img.ImageResource;
import ch.joel.client.ui.img.ImageResource.Images;
import ch.joel.languages.LanguageHandler;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

public class LoginDialog {

	public boolean okButton = false;
	public boolean toRegistration = false;

	private boolean hasUsername;
	private boolean hasPassword;
	private boolean hasServer;
	private boolean hasPort;

	public String[] loginDialog(String user, String password, String host, String port, boolean keepLogged) {
		okButton = false;
		LanguageHandler lang = Client.getInstance().getLangHandler();
		Dialog<Pair<String, String>> dialog = new Dialog<>();
		dialog.setTitle(lang.get("login"));
		dialog.setHeaderText(lang.get("logintext"));

		// Set the icon (must be included in the project).
		dialog.setGraphic(new ImageView(new Image(ImageResource.getFilePath(Images.LOGIN))));

		// Set the button types.
		ButtonType loginButtonType = new ButtonType(lang.get("login"), ButtonData.OK_DONE);
		ButtonType registerButtonType = new ButtonType(lang.get("register"), ButtonData.LEFT);
		ButtonType cancel = new ButtonType(lang.get("cancel"), ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().addAll(registerButtonType, loginButtonType, cancel);

		// Create the username and password labels and fields.
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField usernameField = new TextField();
		usernameField.setPromptText(lang.get("username"));
		usernameField.setText(user);
		PasswordField passwordField = new PasswordField();
		passwordField.setPromptText(lang.get("password"));
		passwordField.setText(password);
		TextField serverField = new TextField();
		serverField.setPromptText(lang.get("host"));
		serverField.setText(host);
		TextField portField = new TextField();
		portField.setPromptText(lang.get("port"));
		portField.setText(port);
		CheckBox keepLoggedField = new CheckBox();
		keepLoggedField.setText(lang.get("keeplogged"));
		keepLoggedField.setAllowIndeterminate(false);
		keepLoggedField.setSelected(keepLogged);

		grid.add(new Label(lang.get("username") + ":"), 0, 0);
		grid.add(usernameField, 1, 0);
		grid.add(new Label(lang.get("password") + ":"), 0, 1);
		grid.add(passwordField, 1, 1);
		grid.add(new Label(lang.get("host") + ":"), 0, 2);
		grid.add(serverField, 1, 2);
		grid.add(new Label(lang.get("port") + ":"), 0, 3);
		grid.add(portField, 1, 3);
		grid.add(keepLoggedField, 2, 3);

		// Enable/Disable login button depending on whether a username was
		// entered.
		Button registerButton = (Button) dialog.getDialogPane().lookupButton(registerButtonType);
		registerButton.setOnAction((event) -> {
			toRegistration = true;
			Platform.runLater(() -> Client.getInstance().showRegisterOverview());

		});

		Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
		loginButton.setDisable(true);

		// Do some validation (using the Java 8 lambda syntax).
		usernameField.textProperty().addListener((observable, oldValue, newValue) -> {
			setUsername(!newValue.trim().isEmpty());
			tryActivate(loginButton);

		});

		passwordField.textProperty().addListener((observable, oldValue, newValue) -> {
			setPassword(!newValue.trim().isEmpty());
			tryActivate(loginButton);
		});

		serverField.textProperty().addListener((observable, oldValue, newValue) -> {
			setHost(!newValue.trim().isEmpty());
			tryActivate(loginButton);
		});

		portField.textProperty().addListener((observable, oldValue, newValue) -> {
			setPort(!newValue.trim().isEmpty());
			tryActivate(loginButton);
		});

		if (usernameField.getText() != null && !usernameField.getText().isEmpty()) {
			setUsername(true);
		}
		if (passwordField.getText() != null && !passwordField.getText().isEmpty()) {
			setPassword(true);
		}
		if (serverField.getText() != null && !serverField.getText().isEmpty()) {
			setHost(true);
		}
		if (portField.getText() != null && !portField.getText().isEmpty()) {
			setPort(true);
		}
		tryActivate(loginButton);

		dialog.getDialogPane().setContent(grid);

		// Request focus on the username field by default.
		Platform.runLater(() -> {
			usernameField.requestFocus();
		});

		// Convert the result to a username-password-pair when the login button
		// is clicked.
		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == loginButtonType) {
				okButton = true;
				return new Pair<>(usernameField.getText(), serverField.getText());
			}
			return null;
		});

		Optional<Pair<String, String>> result = dialog.showAndWait();

		String[] userServer = new String[5];
		result.ifPresent(usernameServer -> {
			userServer[0] = usernameServer.getKey();
			userServer[1] = passwordField.getText();
			userServer[2] = usernameServer.getValue();
			userServer[3] = portField.getText();
			userServer[4] = Boolean.toString(keepLoggedField.isSelected());
		});
		if (okButton) {
			return userServer;
		} else {
			return null;
		}
	}

	public String[] loginDialog() {
		return loginDialog(null, null, null, null, false);
	}

	private void tryActivate(Node loginButton) {
		if (this.hasUsername && this.hasPassword && this.hasServer && this.hasPort) {
			loginButton.setDisable(false);
		} else {
			loginButton.setDisable(true);
		}
	}

	private void setUsername(boolean bool) {
		hasUsername = bool;
	}

	private void setPassword(boolean bool) {
		hasPassword = bool;
	}

	private void setHost(boolean bool) {
		hasServer = bool;
	}

	private void setPort(boolean bool) {
		hasPort = bool;
	}
}
