package ch.joel.client.ui;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Base64;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;
import com.vdurmont.emoji.EmojiParser;

import ch.joel.client.Client;
import ch.joel.client.session.ClientSession;
import ch.joel.client.session.FileHandler;
import ch.joel.languages.LanguageHandler;
import ch.joel.util.Constants;
import ch.joel.util.UserProperties;
import impl.org.controlsfx.autocompletion.SuggestionProvider;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.stage.FileChooser;

public class SettingsHandler {

	private LanguageHandler langHandler;
	private ScrollPane chatScrollPane;
	private ClientSession session;

	public SettingsHandler(ScrollPane chatScrollPane) {
		langHandler = Client.getInstance().getLangHandler();
		this.chatScrollPane = chatScrollPane;

	}

	public void setSession(ClientSession session) {
		this.session = session;
	}

	public void handleBackground() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Images", "*.jpg", "*.png"));
		fileChooser.setTitle(langHandler.get("choosebg"));
		File file = Client.getInstance().openDialog(fileChooser);
		if (file != null) {
			File target = new File(FileHandler.getDefaultPath() + "background.jpg");
			try {
				Files.copy(file.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				e.printStackTrace();
			}
			updateBackground();
		}
	}

	public void updateBackground() {
		File bg = new File(FileHandler.getDefaultPath() + "background.jpg");
		if (bg.exists()) {
			try {
				chatScrollPane.setBackground(new Background(new BackgroundImage(new Image(bg.toURI().toURL().toString()), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(chatScrollPane.getWidth(), chatScrollPane.getHeight(), false, false, false, true))));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}

	}

	public void handleProfile() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Images", "*.jpg", "*.png", "*.gif"));
		fileChooser.setTitle(langHandler.get("chooseprofile"));
		File file = Client.getInstance().openDialog(fileChooser);
		if (file != null) {
			clearPicture();
			File target = new File(FileHandler.getDefaultPath() + "profile." + FilenameUtils.getExtension(file.getPath()));
			try {
				Files.copy(file.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				e.printStackTrace();
			}
			updatePicture(FilenameUtils.getExtension(target.getPath()));
		}
	}

	private void clearPicture() {
		File target = new File(FileHandler.getDefaultPath() + "profile.gif");
		if (target.exists()) {
			target.delete();
		}
		target = new File(FileHandler.getDefaultPath() + "profile.jpg");
		if (target.exists()) {
			target.delete();
		}
		target = new File(FileHandler.getDefaultPath() + "profile.png");
		if (target.exists()) {
			target.delete();
		}

	}

	private boolean updatePicture(String extension) {
		File picture = new File(FileHandler.getDefaultPath() + "profile." + extension);
		if (picture.exists()) {
			try {
				Client.getInstance().getController().setUserImage(new Image(picture.toURI().toURL().toString(), 50, 50, true, true));
				session.sendPbUpdate(Base64.getEncoder().encodeToString(Files.readAllBytes(Paths.get(picture.getPath()))));
				return true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;

	}

	public void handlePassword() {
		Optional<String> password = showInputDialog(langHandler.get("password"), langHandler.get("enterpassword"), langHandler.get("password"));

		if (!password.isPresent()) {
			return;
		}
		if (Client.getInstance().checkPassword(password.get())) {

			session.sendNewPaswd(password.get());
			;
		} else {
			ExceptionDialog.showDelayedException(langHandler.get("passwordinvalid"));
		}

	}

	public Optional<String> showInputDialog(String title, String header, String attribute) {
		TextInputDialog dialog = new TextInputDialog();
		dialog.setContentText(attribute);
		dialog.setTitle(title);
		dialog.setHeaderText(header);
		return dialog.showAndWait();
	}

	public void handleStatus() {

		TextInputDialog dialog = new TextInputDialog();
		dialog.setContentText(langHandler.get("status"));
		dialog.setTitle(langHandler.get("status"));
		dialog.setHeaderText(langHandler.get("enterstatus"));
		EditedAutoCompletionTextField auto = new EditedAutoCompletionTextField(dialog.getEditor(), SuggestionProvider.create(Client.getInstance().getController().getEmojiAliases()));
		dialog.getEditor().textProperty().addListener((obs, oldV, newV) -> {
			int at = 0;
			if (oldV.length() < newV.length()) {
				at = dialog.getEditor().getCaretPosition();
			} else {
				at = dialog.getEditor().getCaretPosition() - 1;
			}
			at++;
			at = Math.min(at, newV.length());
			Matcher mtch = Pattern.compile(".*(\\([^\\)]+)").matcher(newV.substring(0, at));
			if (mtch.matches()) {
				String update = mtch.group(1);
				auto.setUserInput(update);

			} else {
				auto.setUserInput("");
			}

			Matcher matcher = Constants.EMOJIP.matcher(newV);
			if (matcher.matches()) {
				new Thread() {
					@Override
					public void run() {
						do {
							String name = matcher.group(1);
							Emoji emoji = EmojiManager.getForAlias(name);
							if (emoji != null) {
								final String typed = newV.replaceFirst("\\(" + name + "\\)", emoji.getUnicode());
								Platform.runLater(() -> {
									dialog.getEditor().setText(typed);
									dialog.getEditor().positionCaret(dialog.getEditor().getText().length());
								});
							}
						} while (matcher.find());
					}
				}.start();
			}
		});
		Optional<String> status = dialog.showAndWait();

		if (!status.isPresent()) {
			return;
		}
		String text = status.get();
		if (status.get().length() > 120) {
			ExceptionDialog.showException(langHandler.get("smallerstatus"));
			return;
		}
		text = EmojiParser.parseToAliases(text);
		session.sendStatusUpdate(text);

	}

	public void handleDelete() {
		boolean isOkay = showConfirmationDialog(langHandler.get("deleteaccount"), langHandler.get("accountdeleteconfirm"), langHandler.get("dataremove"));
		if (isOkay) {
			session.sendAccountDelete();
			Client.getInstance().getController().showChat(null);
			try {
				FileUtils.deleteDirectory(new File(FileHandler.getDefaultPath()));
			} catch (IOException e) {
				e.printStackTrace();
			}
			Platform.runLater(new Runnable() {

				@Override
				public void run() {
					Client.getInstance().closeStage();
					Client.getInstance().showLoginOverview();

				}
			});
		}
	}

	public boolean showConfirmationDialog(String title, String head, String text) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle(title);
		alert.setHeaderText(head);
		alert.setContentText(text);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.isPresent()) {
			return result.get() == ButtonType.OK;
		}
		return false;
	}

	public void handleVideo(boolean booleanValue) {
		UserProperties.setShowVideo(Boolean.toString(booleanValue));
		UserProperties.saveValues();
	}

	public void handleEncrypt(boolean booleanValue) {
		UserProperties.setEncrypt(Boolean.toString(booleanValue));
		UserProperties.saveValues();
	}

}
