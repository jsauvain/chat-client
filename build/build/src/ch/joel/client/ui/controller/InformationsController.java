package ch.joel.client.ui.controller;

import java.io.IOException;

import ch.joel.client.Client;
import ch.joel.client.state.ChatPartner;
import ch.joel.client.ui.view.FXMLResource;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

public class InformationsController {

	private Stage stage;
	private AnchorPane root;

	@FXML
	private Label name;
	@FXML
	private ImageView image;
	@FXML
	private Label status;

	public InformationsController(ChatPartner cp) {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(FXMLResource.load(FXMLResource.FXML.Informations));
		loader.setController(this);
		try {
			root = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene(root);
		stage = new Stage();
		stage.setScene(scene);
		stage.getIcons().add(new Image("http://www.iconsdb.com/icons/preview/orange/chat-4-xxl.png"));
		stage.setResizable(false);
		stage.setAlwaysOnTop(true);
		name.setText(cp.getName());
		image.setImage(cp.getImage());
		status.setText(cp.getStatus());
		status.setWrapText(true);
		status.setOnMouseClicked((event) -> {
			if (event.getButton() == MouseButton.SECONDARY) {
				Clipboard clpbrd = Clipboard.getSystemClipboard();
				ClipboardContent cnt = new ClipboardContent();
				cnt.putString(status.getText());
				cnt.putHtml(status.getText());
				clpbrd.setContent(cnt);
				TrayNotification not = new TrayNotification(Client.getInstance().getLangHandler().get("success"), Client.getInstance().getLangHandler().get("textcopied"), NotificationType.SUCCESS);
				not.setAnimationType(AnimationType.POPUP);
				not.showAndDismiss(Duration.seconds(3));
			}

		});

	}

	public void show() {
		stage.show();
	}

}
