package ch.joel.client.ui;

import java.util.Timer;
import java.util.TimerTask;

import ch.joel.client.Client;
import ch.joel.client.state.ChatPartner;
import ch.joel.client.state.ChatPartners;

public class WritingUpdater extends Timer {

	private ChatPartners cps;

	public WritingUpdater(ChatPartners cps) {
		this.cps = cps;
	}

	public void scheduleAtFixedRate() {
		super.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				for (ChatPartner cp : cps.getPartners()) {
					if (cp.getWritingState()) {
						String writing = Client.getInstance().getLangHandler().get("writing");
						final String case1 = writing + ".";
						final String case2 = writing + "..";
						final String case3 = writing + "...";
						String yet = cp.getWriting().get();
						if (yet.equals(case1)) {
							cp.setWritingStyle(case2);
						} else if (yet.equals(case2)) {
							cp.setWritingStyle(case3);
						} else if (yet.equals(case3)) {
							cp.setWritingStyle(case1);
						}
					}
				}
			}
		}, 500, 500);
	}

}
