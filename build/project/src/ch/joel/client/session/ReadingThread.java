package ch.joel.client.session;

import java.io.IOException;
import java.net.SocketException;

import ch.joel.client.Client;
import ch.joel.client.session.ClientSession.ClientState;
import ch.joel.client.ui.ExceptionDialog;
import javafx.application.Platform;

public class ReadingThread extends Thread {

	private ClientSession session;
	private MessageProcessor messageUtil;

	public ReadingThread(ClientSession session, MessageProcessor processor) {
		super("ReadingThread");
		this.messageUtil = processor;
		this.session = session;
	}

	@Override
	public void run() {
		try {
			String input;
			while ((input = session.read()) != null && session.getState() == ClientState.LOGGEDIN) {
				messageUtil.processMessage(input);
			}
		} catch (SocketException e) {
			// going into finally block
		} catch (IOException e) {
			if (session.getState() == ClientState.LOGGEDIN) {
				throw new RuntimeException(e);
			}
		} finally {
			if (session.getState() == ClientState.LOGGEDIN) {

				ExceptionDialog.showDelayedException(Client.getInstance().getLangHandler().get("connectionlost"));
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						session.close();
						Client.getInstance().closeStage();
						Client.getInstance().showLoginOverview();

					}
				});

			}
		}
	}

}
