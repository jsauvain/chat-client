package ch.joel.client.ui.controller;

import static ch.joel.client.message.MessageState.ME;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;
import com.vdurmont.emoji.EmojiParser;

import ch.joel.client.Client;
import ch.joel.client.message.AbstractMessage;
import ch.joel.client.message.FileMessage;
import ch.joel.client.message.ImageMessage;
import ch.joel.client.message.LinkMessage;
import ch.joel.client.message.MessageState;
import ch.joel.client.message.RequestMessage;
import ch.joel.client.message.TextMessage;
import ch.joel.client.message.VideoMessage;
import ch.joel.client.session.ClientSession;
import ch.joel.client.session.FileHandler;
import ch.joel.client.state.ChatPartner;
import ch.joel.client.state.ChatPartners;
import ch.joel.client.ui.CellRenderer;
import ch.joel.client.ui.EditedAutoCompletionTextField;
import ch.joel.client.ui.ExceptionDialog;
import ch.joel.client.ui.FilePresenter;
import ch.joel.client.ui.SettingsHandler;
import ch.joel.client.ui.WritingUpdater;
import ch.joel.client.ui.img.ImageResource;
import ch.joel.client.ui.img.ImageResource.Images;
import ch.joel.languages.LanguageHandler;
import ch.joel.util.Constants;
import ch.joel.util.UserProperties;
import ch.joel.util.WritingTimer;
import impl.org.controlsfx.autocompletion.SuggestionProvider;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseButton;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaPlayer.Status;
import javafx.scene.media.MediaView;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

public class ChatOverviewController {

	private Image userImage;
	private ClientSession session;
	private ChatPartners cp;
	private FilePresenter presenter;
	private ChangeListener<ChatPartner> selectionListener;
	private ChangeListener<Number> scrollListener;
	private ChangeListener<Number> scrollChangeListener;
	private boolean lastRequestSuccessfull = true;
	private LanguageHandler langHandler;
	private SettingsHandler settingsHandler;

	private List<String> allEmojis;

	private EditedAutoCompletionTextField auto;

	@FXML
	private ListView<ChatPartner> chatPartners;
	@FXML
	private Label chatPartnerName;
	@FXML
	private VBox chat;
	@FXML
	private ScrollPane chatScrollPane;
	@FXML
	private TextField text;
	@FXML
	private Button send;
	@FXML
	private AnchorPane chatWindow;
	@FXML
	private ImageView chatPartnerImage;
	@FXML
	private ScrollPane partnerScrollPane;
	@FXML
	private MenuButton menuBtSettings;
	@FXML
	private MenuButton menuBtPartner;
	@FXML
	private SplitPane splitPane;

	public void init(ClientSession session, ChatPartners cp) {
		this.session = session;
		settingsHandler.setSession(session);
		this.cp = cp;
		chatPartners.setItems(cp.getPartners());
		presenter = new FilePresenter(cp);
		langHandler = Client.getInstance().getLangHandler();
		send.setText(langHandler.get("send"));
		List<MenuItem> items = menuBtSettings.getItems();
		items.get(0).setText(langHandler.get("bg"));
		items.get(1).setText(langHandler.get("pp"));
		items.get(2).setText(langHandler.get("changepassword"));
		items.get(3).setText(langHandler.get("deleteaccount"));
		items.get(4).setText(langHandler.get("status"));
		items = menuBtPartner.getItems();
		items.get(0).setText(langHandler.get("informations"));
		allEmojis = new ArrayList<>();
		for (Emoji emoji : EmojiManager.getAll()) {
			for (String stringEmoji : emoji.getAliases()) {
				allEmojis.add("(" + stringEmoji + ")");
			}
		}
		auto = new EditedAutoCompletionTextField(text, SuggestionProvider.create(allEmojis));
		new WritingUpdater(cp).scheduleAtFixedRate();
	}

	@FXML
	private void initialize() {
		settingsHandler = new SettingsHandler(chatScrollPane);
		chatPartners.setCellFactory(new CellRenderer());
		chatPartners.setFixedCellSize(70);
		chatWindow.setVisible(false);
		chatPartnerName.setText(null);
		settingsHandler.updateBackground();
		chatPartnerImage.setClip(new Circle(chatPartnerImage.getX() + chatPartnerImage.getFitWidth() / 2, chatPartnerImage.getY() + chatPartnerImage.getFitHeight() / 2, 25));
		userImage = Client.getProfilePic();
		chatPartners.getSelectionModel().selectedItemProperty().addListener((selectionListener = (observable, oldValue, newValue) -> {
			if (newValue != null) {
				chatScrollPane.vvalueProperty().removeListener(scrollChangeListener);
				changeNameButtonLabel(newValue);
				showChatFromName(newValue);
			}
		}));
		chat.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		chatScrollPane.setContent(chat);

		partnerScrollPane.setContent(chatPartners);

		chat.heightProperty().addListener(scrollListener = (obs, oldV, newV) -> {
			chatScrollPane.setVvalue(newV.doubleValue());
		});

		scrollChangeListener = ((obs, oldV, newV) -> {
			if (newV.doubleValue() <= 0.001D) {
				if (getChatPartnerName() != null) {
					int starting = cp.getChatPartner(getChatPartnerName()).getChatMessages().size();
					if (lastRequestSuccessfull) {
						lastRequestSuccessfull = false;
						session.requestChat(getChatPartnerName(), starting);
					}
				}
			}
		});
		menuBtSettings.setOnMouseClicked((event) -> {
			if (event.getClickCount() == 2) {
				new SettingsController(settingsHandler).show();
				event.consume();
			}
		});
		addtextChangeListener();
		send.setDisable(true);
	}

	public void setSelectedUser(ChatPartner partner) {
		chatPartners.getSelectionModel().select(partner);
	}

	public void addToGUI(ChatPartner cp) {
		for (ChatPartner names : this.cp.getPartners())
			if (cp.getName().equals(names.getName())) {
				cp.setImage(names.getImage());
				return;
			}
		this.cp.addToGUI(cp);
	}

	public Image getUserImage() {
		return userImage;
	}

	@FXML
	public void addFriend() {
		new OnlinePartnersDialog(cp);
	}

	@FXML
	public void deleteAccount() {
		settingsHandler.handleDelete();
	}

	@FXML
	public void changePassword() {
		settingsHandler.handlePassword();
	}

	@FXML
	public void handleDragging(DragEvent event) {
		Dragboard db = event.getDragboard();
		if (db.hasFiles()) {
			event.acceptTransferModes(TransferMode.COPY);
		} else {
			event.consume();
		}
	}

	@FXML
	private void onDragDrop(DragEvent e) {
		Dragboard db = e.getDragboard();
		boolean success = false;
		if (db.hasFiles()) {
			success = true;
			for (File file : db.getFiles()) {
				text.setText(text.getText() + "[FILE:!" + file.getPath() + "!]");
			}
		}
		e.setDropCompleted(success);
		e.consume();

	}

	@FXML
	private void handleDirectDragging(DragEvent event) {
		Dragboard db = event.getDragboard();
		if (db.hasFiles()) {
			event.acceptTransferModes(TransferMode.COPY);
		} else {
			event.consume();
		}
	}

	@FXML
	private void handleDirectDragDrop(DragEvent e) {
		Dragboard db = e.getDragboard();
		boolean success = false;
		if (db.hasFiles()) {
			success = true;
			for (File file : db.getFiles()) {
				Integer id = (int) (Math.random() * 1000000) + 1;
				RequestMessage rqm = new RequestMessage(id, MessageState.ME);
				rqm.setProcess(0);
				cp.getChatPartner(getChatPartnerName()).addMessage(rqm);
				new Thread(() -> {
					session.sendFile(file.getPath(), chatPartnerName.getText(), id, rqm);
				}).start();
			}
		}
		e.setDropCompleted(success);
		e.consume();
	}

	@FXML
	private void changeStatus() {
		settingsHandler.handleStatus();
	}

	public void showChatAndChangeNameLabel(String name) {
		ChatPartner chatPartner = cp.getChatPartner(name);
		if (chatPartner != null) {
			showChatFromName(chatPartner);
			changeNameButtonLabel(chatPartner);
		}
	}

	private void changeNameButtonLabel(ChatPartner newValue) {
		if (newValue != null) {
			chatPartnerName.setText(newValue.getName());
		} else {
			chatPartnerName.setText("");
		}
	}

	private void showChatFromName(ChatPartner newValue) {
		if (newValue != null) {
			showChat(newValue.getChatMessages());
			showImage(newValue.getImage());
			if (newValue.getUnread() > 0) {
				session.sendClearUnread(newValue.getName());
			}
			newValue.clearUnread();
			chatWindow.setVisible(true);

		}
	}

	@FXML
	public void picture() {
		settingsHandler.handleProfile();
	}

	public synchronized void updateChatReplacement(AbstractMessage message, Integer index, boolean atTop) {
		Node node = null;
		if (message instanceof TextMessage) {
			node = prepareTextMessage(message);
		} else if (message instanceof ImageMessage) {
			node = prepareImageMessage(message);
		} else if (message instanceof LinkMessage) {
			node = prepareHyperlink(message);
		} else if (message instanceof VideoMessage) {
			if (UserProperties.showVideos()) {
				node = prepareVideoMessage(message);
			} else {
				node = prepareDefaultVideo(message);
			}
		} else if (message instanceof FileMessage) {
			node = prepareFileMessage(message);
		} else if (message instanceof RequestMessage) {
			node = prepareRequestMessage(message);

		}
		HBox hb = styleMessage(message, node);
		addMessage(hb, index, atTop);
	}

	private Node prepareDefaultVideo(AbstractMessage message) {
		Node node;
		StackPane stackPane = new StackPane();
		stackPane.getStyleClass().add("chat-bubble");
		ImageView imgView = new ImageView(new Image(ImageResource.getFilePath(Images.MEDIA)));
		imgView.setFitHeight(100);
		imgView.setFitWidth(100);
		Button bt = new Button();
		bt.getStyleClass().add("chatMedia");
		bt.setGraphic(imgView);
		bt.setOnMouseClicked((event) -> {
			if (event.getButton() == MouseButton.SECONDARY) {
				try {
					Runtime.getRuntime().exec("explorer.exe /select," + ((VideoMessage) message).getPath());
					return;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			try {
				Desktop.getDesktop().open(new File(((VideoMessage) message).getPath()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		stackPane.getChildren().add(bt);
		node = stackPane;
		return node;
	}

	public synchronized void updateChat(AbstractMessage message, boolean atTop) {
		updateChatReplacement(message, null, atTop);
	}

	private void addMessage(HBox hb, Integer index, boolean atTop) {
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				if (index != null) {
					chat.getChildren().set(index, hb);
				} else {
					if (atTop) {
						chat.getChildren().add(0, hb);
					} else {
						chat.getChildren().add(hb);
					}
				}
			}
		});
	}

	private HBox styleMessage(AbstractMessage message, Node node) {
		HBox hb;
		ImageView img = new ImageView();
		if (message.getMessageState() == ME) {
			node.getStyleClass().add("chat-bubble-me");
			img.setImage(userImage);
			hb = new HBox(node, img);
			hb.setAlignment(Pos.CENTER_RIGHT);
			VBox.setMargin(hb, new Insets(10, 30, 10, 100));
		} else {
			img.setImage(cp.getChatPartner(chatPartnerName.getText()).getImage());
			node.getStyleClass().add("chat-bubble-him");
			hb = new HBox(img, node);
			hb.setAlignment(Pos.CENTER_LEFT);
			VBox.setMargin(hb, new Insets(10, 100, 10, 30));
		}
		return hb;
	}

	private Node prepareRequestMessage(AbstractMessage message) {
		Node node;
		RequestMessage msg = (RequestMessage) message;
		StackPane stackPane = new StackPane();
		stackPane.getStyleClass().add("chat-bubble");
		ImageView imgView = new ImageView(new Image(ImageResource.getFilePath(Images.DOWNLOAD)));
		imgView.setFitHeight(100);
		imgView.setFitWidth(100);
		Button bt = new Button();
		bt.getStyleClass().add("chatMedia");
		bt.setMinHeight(100);
		bt.setMinWidth(100);
		if (msg.getProcess().get() > -1D) {
			final ProgressIndicator pin = new ProgressIndicator();
			pin.progressProperty().bind((msg.getProcess()));
			pin.setMinSize(70, 70);
			pin.getStyleClass().add("file-send");
			bt.setGraphic(pin);
		} else {
			bt.setGraphic(imgView);
		}
		bt.setOnMouseClicked((event) -> {
			if (bt.getGraphic().equals(imgView)) {
				File fl = FileHandler.hasFile(msg.getId() + "");
				if (fl != null) {
					presenter.presentFile(getChatPartnerName(), fl, false, msg.getMessageState(), msg.getId(), true, null);
				} else {
					session.sendFileRequest(msg.getId());
					final ProgressIndicator pin = new ProgressIndicator();
					pin.progressProperty().bind(msg.getProcess());
					pin.getStyleClass().add("file-send");
					pin.setMinSize(70, 70);
					bt.setGraphic(pin);
				}

			}
		});
		stackPane.getChildren().add(bt);
		node = stackPane;
		return node;
	}

	private Node prepareFileMessage(AbstractMessage message) {
		Node node;
		StackPane stackPane = new StackPane();
		stackPane.getStyleClass().add("chat-bubble");
		ImageView imgView = new ImageView(new Image(ImageResource.getFilePath(Images.FILE)));
		imgView.setFitHeight(100);
		imgView.setFitWidth(100);
		Button bt = new Button();
		bt.getStyleClass().add("chatMedia");
		bt.setGraphic(imgView);
		bt.setOnMouseClicked((event) -> {
			try {
				Runtime.getRuntime().exec("explorer.exe /select," + ((FileMessage) message).getFile().getPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		stackPane.getChildren().add(bt);
		node = stackPane;
		return node;
	}

	private Node prepareVideoMessage(AbstractMessage message) {
		Node node;
		StackPane stackPane = new StackPane();
		stackPane.getStyleClass().add("chat-bubble");
		MediaPlayer player = ((VideoMessage) message).getMediaPlayer();
		player.setAutoPlay(false);
		player.setMute(false);
		MediaView mdView = new MediaView(player);
		mdView.setFitHeight(200);
		mdView.setFitWidth(200);
		Button bt = new Button();
		bt.getStyleClass().add("chatMedia");
		bt.setGraphic(mdView);
		bt.setOnMouseClicked((event) -> {
			if (event.getButton() == MouseButton.SECONDARY) {
				try {
					Runtime.getRuntime().exec("explorer.exe /select," + ((VideoMessage) message).getPath());
					return;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (event.getClickCount() == 1) {
				if (player.getStatus() == Status.PLAYING) {
					player.pause();
				} else {
					player.play();
				}
			} else if (event.getClickCount() == 2) {
				player.pause();
				try {
					Desktop.getDesktop().open(new File(((VideoMessage) message).getPath()));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		stackPane.getChildren().add(bt);
		node = stackPane;
		return node;
	}

	private Node prepareHyperlink(AbstractMessage message) {
		Node node;
		Hyperlink hyperLink = ((LinkMessage) message).getHyperlink();
		hyperLink.setMinHeight(Region.USE_PREF_SIZE);
		hyperLink.setWrapText(true);
		hyperLink.getStyleClass().add("chat-bubble");
		hyperLink.setOnMouseClicked((event) -> {
			if (event.getButton() == MouseButton.SECONDARY) {
				Clipboard clpbrd = Clipboard.getSystemClipboard();
				ClipboardContent cnt = new ClipboardContent();
				cnt.putString(((LinkMessage) message).getHyperlink().getText());
				cnt.putHtml(((LinkMessage) message).getHyperlink().getText());
				clpbrd.setContent(cnt);
				TrayNotification not = new TrayNotification(langHandler.get("success"), langHandler.get("linkcopied"), NotificationType.SUCCESS);
				not.setAnimationType(AnimationType.POPUP);
				not.showAndDismiss(Duration.seconds(3));
				return;
			}
			try {
				Desktop.getDesktop().browse(new URI(hyperLink.getText()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		node = hyperLink;
		return node;
	}

	private Node prepareImageMessage(AbstractMessage message) {
		Node node;
		StackPane stackPane = new StackPane();
		stackPane.getStyleClass().add("chat-bubble");
		ImageView imgView = new ImageView(((ImageMessage) message).getImage());
		imgView.setFitHeight(imgView.getImage().getHeight() * 2);
		imgView.setFitWidth(imgView.getImage().getWidth() * 2);
		Button bt = new Button();
		bt.getStyleClass().add("chatMedia");
		bt.setGraphic(imgView);
		bt.setOnMouseClicked((event) -> {
			if (event.getButton() == MouseButton.SECONDARY) {
				try {
					Runtime.getRuntime().exec("explorer.exe /select," + ((ImageMessage) message).getPath());
					return;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			try {
				Desktop.getDesktop().open(new File(((ImageMessage) message).getPath()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		stackPane.getChildren().add(bt);
		node = stackPane;
		return node;
	}

	private Label prepareTextMessage(AbstractMessage message) {
		Label chatMessage = new Label(((TextMessage) message).getMessage());
		chatMessage.setMinHeight(Region.USE_PREF_SIZE);
		chatMessage.setWrapText(true);
		chatMessage.getStyleClass().add("chat-bubble");
		chatMessage.setOnMouseClicked((event) -> {
			if (event.getButton() == MouseButton.SECONDARY) {
				Clipboard clpbrd = Clipboard.getSystemClipboard();
				ClipboardContent cnt = new ClipboardContent();
				cnt.putString(((TextMessage) message).getMessage());
				cnt.putHtml(((TextMessage) message).getMessage());
				clpbrd.setContent(cnt);
				TrayNotification not = new TrayNotification(langHandler.get("success"), langHandler.get("textcopied"), NotificationType.SUCCESS);
				not.setAnimationType(AnimationType.POPUP);
				not.showAndDismiss(Duration.seconds(3));
			}
		});
		return chatMessage;
	}

	public synchronized void showChat(List<AbstractMessage> messages) {
		chat.getChildren().clear();
		if (messages != null) {

			List<AbstractMessage> messagesCopy = new ArrayList<>(messages);
			new Thread(new Runnable() {
				@Override
				public void run() {
					for (AbstractMessage message : messagesCopy) {
						updateChat(message, false);
					}
					new Timer().schedule(new TimerTask() {

						@Override
						public void run() {
							Platform.runLater(() -> {
								text.requestFocus();
								chatScrollPane.vvalueProperty().addListener(scrollChangeListener);
							});
						}
					}, 100);

				}
			}).start();
		} else {
			chatWindow.setVisible(false);
		}
	}

	public void showImage(Image image) {
		chatPartnerImage.setImage(image);
	}

	private boolean isAlreadyWriting = false;

	private void addtextChangeListener() {
		text.textProperty().addListener(((obs, oldV, newV) -> {
			int at = 0;
			if (oldV.length() < newV.length()) {
				at = text.getCaretPosition();
			} else {
				at = text.getCaretPosition() - 1;
			}
			at++;
			at = Math.min(at, newV.length());
			Matcher mtch = Pattern.compile(".*(\\([^\\)]+)").matcher(newV.substring(0, at));
			if (mtch.matches()) {
				String update = mtch.group(1);
				auto.setUserInput(update);

			} else {
				auto.setUserInput("");
			}
			Matcher matcher = Constants.EMOJIP.matcher(newV);
			if (matcher.matches()) {
				new Thread() {
					@Override
					public void run() {
						do {
							String name = matcher.group(1);
							Emoji emoji = EmojiManager.getForAlias(name);
							if (emoji != null) {
								final String typed = newV.replaceFirst("\\(" + name + "\\)", emoji.getUnicode());
								Platform.runLater(() -> {
									text.setText(typed);
									text.positionCaret(text.getText().length());
								});
							}
						} while (matcher.find());
					}
				}.start();
			}
			if (newV.equals("/emoji")) {
				try {
					Desktop.getDesktop().browse(new URI("https://github.com/vdurmont/emoji-java#available-emojis"));
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			if (oldV.length() < newV.length()) {

				if (!newV.trim().isEmpty()) {
					send.setDisable(false);
				}
				if (!isAlreadyWriting) {
					session.sendWritingState(true, getChatPartnerName());
					isAlreadyWriting = true;
				}
				WritingTimer.disable();
				new WritingTimer().schedule(new TimerTask() {

					@Override
					public void run() {
						session.sendWritingState(false, getChatPartnerName());
						isAlreadyWriting = false;
					}
				});
			} else {
				if (newV.trim().isEmpty()) {
					send.setDisable(true);
				}
			}
		}));

	}

	@FXML
	private void handleSend() {
		String msg = text.getText();
		String name = chatPartnerName.getText();
		if (!msg.equals("") && name != null && !name.equals("")) {
			text.clear();
			if (session != null) {
				Matcher m = Constants.PATTERN.matcher(msg);
				if (m.matches()) {
					String path = m.group(1);
					File fl = new File(path);
					if (fl.exists()) {
						String partner = chatPartnerName.getText();
						Integer num = (int) (Math.random() * 1000000) + 1;
						RequestMessage rqm = new RequestMessage(num, MessageState.ME);
						rqm.setProcess(0);
						cp.getChatPartner(getChatPartnerName()).addMessage(rqm);
						new Thread(() -> {
							session.sendFile(path, partner, num, rqm);
						}).start();
					} else {
						ExceptionDialog.showException(langHandler.get("filenotfound"));
					}
					msg = msg.replaceAll(Constants.REPLACEMENT, "");
				}
				if (!msg.isEmpty()) {
					Matcher matcherHyper = Constants.HYPERPATTERN.matcher(msg);
					String withoutLink = msg;
					String link = "";
					boolean hasHyper = false;
					if (matcherHyper.matches()) {
						hasHyper = true;
						link = matcherHyper.group(1);
						withoutLink = msg.replace(link, "");

					}
					if (!withoutLink.isEmpty()) {
						cp.getChatPartner(name).addMessage(new TextMessage(withoutLink, ME));
					}
					if (hasHyper) {
						cp.getChatPartner(name).addMessage(new LinkMessage(new Hyperlink(link), ME));
					}
					msg = EmojiParser.parseToAliases(msg);
					session.sendMessage(msg, name);
				}
				WritingTimer.disable();
			}
		}
	}

	@FXML
	private void handleInformations() {
		InformationsController controller = new InformationsController(cp.getChatPartner(getChatPartnerName()));
		controller.show();

	}

	@FXML
	private void handleAttachment() {
		if (!Constants.PATTERN.matcher(text.getText()).matches()) {
			FileChooser fileChooser = new FileChooser();
			fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All Files", "*"), new FileChooser.ExtensionFilter("Images", "*.jpg", "*.png", "*.gif"));
			fileChooser.setTitle("Open Resource File");
			File file = Client.getInstance().openDialog(fileChooser);
			if (file != null) {
				text.setText(text.getText() + "[FILE:!" + file.getPath() + "!]");
			}
		}
	}

	@FXML
	private void background() {
		settingsHandler.handleBackground();
	}

	public ChatPartner getSelectedUser() {
		return chatPartners.getSelectionModel().getSelectedItem();
	}

	public String getChatPartnerName() {
		return chatPartnerName.getText();
	}

	public void setChatPartnerImage(Image img) {
		chatPartnerImage.setImage(img);
	}

	public void updateList() {
		chatPartners.setCellFactory(new CellRenderer());

	}

	public void removeSelectionListener() {
		chatPartners.getSelectionModel().selectedItemProperty().removeListener(selectionListener);
	}

	public void addSelectionListener() {
		chatPartners.getSelectionModel().selectedItemProperty().addListener(selectionListener);
	}

	public void removeScrollListener() {
		chat.heightProperty().removeListener(scrollListener);
	}

	public void addScrollListener() {
		new Timer().schedule(new TimerTask() {

			@Override
			public void run() {
				chat.heightProperty().addListener(scrollListener);

			}
		}, 50);
	}

	public void setRequestSuccessfull() {
		if (!lastRequestSuccessfull) {
			new Timer().schedule(new TimerTask() {

				@Override
				public void run() {
					lastRequestSuccessfull = true;

				}
			}, 50);

		}
	}

	public void clearChatPartnerName() {
		chatPartnerName.setText(null);

	}

	public ChatPartners getChatPartners() {
		return cp;
	}

	public void setUserImage(Image image) {
		this.userImage = image;
	}

	public List<String> getEmojiAliases() {
		return allEmojis;
	}

}
