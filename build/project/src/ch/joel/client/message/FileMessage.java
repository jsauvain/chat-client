package ch.joel.client.message;

import java.io.File;

public class FileMessage extends AbstractMessage implements LargeFileId, PathedMessage {

	private File fl;
	private Integer largeFileId;

	public FileMessage(File fl, MessageState state, Integer largeFileId) {
		super(state);
		this.fl = fl;
		this.largeFileId = largeFileId;

	}

	public File getFile() {
		return fl;
	}

	@Override
	public Integer getLargeFileId() {
		return largeFileId;
	}

	@Override
	public String getPath() {
		return fl.getPath();
	}

}
