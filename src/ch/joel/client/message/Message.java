package ch.joel.client.message;

public interface Message {
	
	public MessageState getMessageState();
}
