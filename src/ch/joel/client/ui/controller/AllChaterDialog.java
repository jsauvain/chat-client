package ch.joel.client.ui.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.textfield.TextFields;

import ch.joel.client.Client;
import ch.joel.client.state.ChatPartner;
import ch.joel.client.state.ChatPartners;
import ch.joel.client.ui.ExceptionDialog;
import ch.joel.client.ui.view.FXMLResource;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class AllChaterDialog {

	private ChatPartners cp;
	private Stage stage;
	private AnchorPane root;
	
	@FXML
	private Label label;
	@FXML
	private TextField textField;
	@FXML
	private Button cancel;

	public AllChaterDialog(ChatPartners cp) {
		this.cp = cp;
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(FXMLResource.load(FXMLResource.FXML.AllChater));
		loader.setController(this);
		try {
			root = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene(root);
		stage = new Stage();
		stage.setScene(scene);
		stage.setMinHeight(300);
		stage.setMinWidth(300);
		stage.setMaxHeight(400);
		stage.setAlwaysOnTop(true);
		label.setText(Client.getInstance().getLangHandler().get("whotoadd"));
		cancel.setText(Client.getInstance().getLangHandler().get("cancel"));
		stage.getIcons().add(new Image("http://www.iconsdb.com/icons/preview/orange/chat-4-xxl.png"));
		List<String> all = new ArrayList<>(Client.getInstance().getAll());
		for (ChatPartner chatPartner : cp.getPartners()) {
			all.remove(chatPartner.getName());
		}
		TextFields.bindAutoCompletion(textField, all);
		stage.show();

	}

	@FXML
	private void okBt() {
		if (validateText()) {
			ChatPartner cp = new ChatPartner(textField.getText(), null);
			if (this.cp.getChatPartner(cp.getName()) != null) {
				cp = this.cp.getChatPartner(cp.getName());
			}
			this.cp.addToGUI(cp);
			Client.getInstance().getSession().sendImageRequest(cp.getName());
			Client.getInstance().getAll().remove(cp.getName());
			stage.close();
		} else {
			ExceptionDialog.showException(Client.getInstance().getLangHandler().get("wronginput"));
		}
	}

	private boolean validateText() {
		String text = textField.getText();
		if (text != null && !text.trim().isEmpty() && Client.getInstance().getAll().contains(text)) {
			return true;
		}
		return false;

	}

	@FXML
	private void cancelBt() {
		stage.close();
	}

}
