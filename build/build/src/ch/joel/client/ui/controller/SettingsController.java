package ch.joel.client.ui.controller;

import java.io.IOException;

import ch.joel.client.Client;
import ch.joel.client.ui.SettingsHandler;
import ch.joel.client.ui.view.FXMLResource;
import ch.joel.languages.LanguageHandler;
import ch.joel.util.UserProperties;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class SettingsController {

	private Stage stage;
	private AnchorPane root;
	private SettingsHandler settingsHandler;

	@FXML
	private Label label;
	@FXML
	private Button background;
	@FXML
	private Button profile;
	@FXML
	private Button password;
	@FXML
	private Button status;
	@FXML
	private Button delete;
	@FXML
	private CheckBox video;
	@FXML
	private CheckBox encrypt;

	public SettingsController(SettingsHandler settingsHandler) {
		this.settingsHandler = settingsHandler;
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(FXMLResource.load(FXMLResource.FXML.Settings));
		loader.setController(this);
		try {
			root = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene(root);
		stage = new Stage();
		stage.setScene(scene);
		stage.getIcons().add(new Image("http://www.iconsdb.com/icons/preview/orange/chat-4-xxl.png"));
		stage.setMaxHeight(500);
		stage.setMinHeight(400);
		stage.setMinWidth(300);
		stage.setAlwaysOnTop(true);
		translate();
		listeners();
		init();

	}

	private void init() {
		video.setSelected(UserProperties.showVideos());
		encrypt.setSelected(UserProperties.doEncryptions());
	}

	private void listeners() {
		background.setOnAction(event -> {
			stage.close();
			settingsHandler.handleBackground();
		});
		profile.setOnAction(event -> {
			stage.close();
			settingsHandler.handleProfile();
		});
		password.setOnAction(event -> {
			stage.setAlwaysOnTop(false);
			settingsHandler.handlePassword();
			stage.setAlwaysOnTop(true);
		});
		status.setOnAction(event -> {
			stage.setAlwaysOnTop(false);
			settingsHandler.handleStatus();
			stage.setAlwaysOnTop(true);
		});
		delete.setOnAction(event -> {
			stage.setAlwaysOnTop(false);
			settingsHandler.handleDelete();
			stage.close();
		});
		video.selectedProperty().addListener((event, oldV, newV) -> {
			settingsHandler.handleVideo(newV.booleanValue());
		});
		encrypt.selectedProperty().addListener((event, oldV, newV) -> {
			settingsHandler.handleEncrypt(newV.booleanValue());
		});

	}

	private void translate() {
		LanguageHandler langHanlder = Client.getInstance().getLangHandler();
		label.setText(langHanlder.get("settings"));
		background.setText(langHanlder.get("bg"));
		profile.setText(langHanlder.get("pp"));
		password.setText(langHanlder.get("password"));
		status.setText(langHanlder.get("status"));
		delete.setText(langHanlder.get("deleteaccount"));
		video.setText(langHanlder.get("showmedia"));
		encrypt.setText(langHanlder.get("encrypt"));

	}

	public void show() {
		stage.show();
	}
}
