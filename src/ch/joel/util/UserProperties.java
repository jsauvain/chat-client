package ch.joel.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class UserProperties {

	private static Properties prop;
	private static File fl;

	private static String keepLogged;
	private static String user;
	private static String pass;
	private static String host;
	private static String port;
	private static String showVideo;
	private static String language;
	private static String encrypt;

	private final String defaultEncrypt = "true";
	private final String defaultkeepLogged = "false";
	private final String defaultUser = "user";
	private final String defaultPass = "password";
	private final String defaultHost = "localhost";
	private final String defaultPort = "4444";
	private final String defaultShowVideo = "true";
	private final String defaultLanguage = "de";

	public void loadProperties() {

		prop = new Properties();

		fl = new File(System.getProperty("user.home") + "/AppData/Local/ChatClient/");
		fl.mkdirs();
		fl = new File(System.getProperty("user.home") + "/AppData/Local/ChatClient/config.properties");
		if (fl.exists()) {
			loadValues(fl);
		} else {
			setDefaultValues(fl);
		}

	}

	private void setDefaultValues(File fl) {
		host = defaultHost;
		user = defaultUser;
		pass = defaultPass;
		port = defaultPort;
		showVideo = defaultShowVideo;
		keepLogged = defaultkeepLogged;
		language = defaultLanguage;
		encrypt = defaultEncrypt;

		try (OutputStream output = new FileOutputStream(fl)) {

			// set the properties value
			prop.setProperty("host", defaultHost);
			prop.setProperty("user", defaultUser);
			prop.setProperty("pass", defaultPass);
			prop.setProperty("port", defaultPort);
			prop.setProperty("keepLogged", defaultkeepLogged);
			prop.setProperty("showVideo", defaultShowVideo);
			prop.setProperty("Language", defaultLanguage);
			prop.setProperty("encrypt", defaultEncrypt);

			// save properties to project root folder
			prop.store(output, null);

		} catch (IOException io) {
			io.printStackTrace();
		}
	}

	private void loadValues(File fl) {
		try (InputStream input = new FileInputStream(fl)) {
			prop.load(input);
			host = prop.getProperty("host");
			user = prop.getProperty("user");
			pass = prop.getProperty("pass");
			port = prop.getProperty("port");
			keepLogged = prop.getProperty("keepLogged");
			showVideo = prop.getProperty("showVideo");
			language = prop.getProperty("Language");
			encrypt = prop.getProperty("encrypt");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void saveValues() {
		try (OutputStream output = new FileOutputStream(fl)) {

			// set the properties value
			prop.setProperty("host", host);
			prop.setProperty("user", user);
			prop.setProperty("pass", pass);
			prop.setProperty("port", port);
			prop.setProperty("keepLogged", keepLogged);
			prop.setProperty("showVideo", showVideo);
			prop.setProperty("encrypt", encrypt);

			// save properties to project root folder
			prop.store(output, null);

		} catch (IOException io) {
			io.printStackTrace();
		}
	}

	public static String getHost() {
		return host;
	}

	public static String getUser() {
		return user;
	}

	public static String getPass() {
		return pass;
	}

	public static String getPort() {
		return port;
	}

	public static String getLoggedState() {
		return keepLogged;
	}

	public static String getKeepLogged() {
		return keepLogged;
	}

	public static boolean showVideos() {
		return Boolean.valueOf(showVideo);
	}

	public static String getLanguage() {
		return language;
	}

	public static boolean doEncryptions() {
		return Boolean.valueOf(encrypt);
	}

	public static void setLogin(String keepLogged, String user, String pass, String host, String port) {
		setKeepLogged(keepLogged);
		setUser(user);
		setPass(pass);
		setHost(host);
		setPort(port);
	}

	public static void setShowVideo(String bool) {
		showVideo = bool;
	}

	public static void setKeepLogged(String keepLogged) {
		UserProperties.keepLogged = keepLogged;
	}

	public static void setUser(String user) {
		UserProperties.user = user;
	}

	public static void setPass(String pass) {
		UserProperties.pass = pass;
	}

	public static void setHost(String host) {
		UserProperties.host = host;
	}

	public static void setPort(String port) {
		UserProperties.port = port;
	}

	public static void setEncrypt(String encrypt) {
		UserProperties.encrypt = encrypt;
	}

	public static void clear() {

		user = "user";
		pass = "password";
		host = "localhost";
		port = "4444";

	}

}
