package ch.joel.util;

import java.util.regex.Pattern;

public class Constants {

	public final static int LOGIN = 1;
	public final static int LOGOUT = 2;
	public final static int TEXT = 3;
	public final static int FILE = 4;
	public final static int ERROR = 5;
	public final static int UPDATEPB = 6;
	public final static int REGISTER = 7;
	public final static int LARGEFILE = 8;
	public final static int DELETEACCOUNT = 9;
	public final static int WRITING = 10;
	public final static int CHANGEPASSWORD = 11;
	public final static int ALLUSER = 12;
	public final static int REQUESTIMAGE = 13;
	public final static int SAVEFRIENDS = 14;
	public final static int REQUESTCHAT = 15;
	public final static int CLEARUNREAD = 16;
	public final static int PREVIEW = 17;
	public final static int REQUESTFILE = 18;
	public final static int FILEID = 19;
	public final static int PUBLICKEY = 20;
	public final static int SYMKEY = 21;
	public static final int REQUESTSTATUS = 22;
	public static final int UPDATESTATUS = 23;
	
	public final static String SEPERATOR = ((char) 007) + "";

	public final static Pattern EMOJIP = Pattern.compile(".*\\((.*)\\).*");
	public final static String EMOJIRP = "\\(.*\\)";
	public final static Pattern PATTERN = Pattern.compile(".*\\[FILE:!(.*)!\\].*");
	public final static String REPLACEMENT = "\\[FILE:!.*!\\]";
	public final static Pattern HYPERPATTERN = Pattern.compile(".*(https?:\\/\\/.*\\.[^\\s]*).*");
}
