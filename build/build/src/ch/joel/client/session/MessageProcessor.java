package ch.joel.client.session;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import com.vdurmont.emoji.EmojiParser;

import ch.joel.client.Client;
import ch.joel.client.message.AbstractMessage;
import ch.joel.client.message.LargeFileId;
import ch.joel.client.message.LinkMessage;
import ch.joel.client.message.MessageState;
import ch.joel.client.message.PathedMessage;
import ch.joel.client.message.RequestMessage;
import ch.joel.client.message.TextMessage;
import ch.joel.client.state.ChatPartner;
import ch.joel.client.state.ChatPartners;
import ch.joel.client.ui.ExceptionDialog;
import ch.joel.client.ui.FilePresenter;
import ch.joel.util.Constants;
import javafx.application.Platform;
import javafx.scene.control.Hyperlink;
import javafx.scene.image.Image;

public class MessageProcessor {

	private ChatPartners chatPartners;
	private Map<Integer, FileHandler> fileHandlers;
	private FilePresenter presenter;
	private ClientSession session;
	private boolean gotPublic = false;

	public MessageProcessor(ChatPartners cps, ClientSession session) {
		this.chatPartners = cps;
		this.session = session;
		fileHandlers = new HashMap<>();
		presenter = new FilePresenter(chatPartners);

	}

	public void processMessage(String message) {
		if (!gotPublic) {
			String[] splitMsg = message.split(Constants.SEPERATOR);
			if (splitMsg.length == 2 && Integer.parseInt(splitMsg[0]) == Constants.PUBLICKEY) {
				handlePublicKey(splitMsg[1]);
				gotPublic = true;
			}
		} else {
			boolean encrypted = true;
			if (message.length() >= 2) {
				String first = message.substring(0, 2);
				if ((first.toCharArray()[1] + "").equals(Constants.SEPERATOR)) {
					encrypted = false;
				}
			}
			String decoded = encrypted ? session.decode(message) : message;
			String[] splitMsg = decoded.split(Constants.SEPERATOR);
			switch (Integer.parseInt(splitMsg[0])) {
				case Constants.LOGIN:
					handleLogin(splitMsg);
					break;
				case Constants.LOGOUT:
					handleLogout(splitMsg[1]);
					break;
				case Constants.TEXT:
					handleText(splitMsg[2], splitMsg[3], true, MessageState.PARTNER, false);
					break;
				case Constants.FILE:
					handleSmallFile(splitMsg[3], splitMsg[2], splitMsg[4], true, MessageState.PARTNER, false);
					break;
				case Constants.LARGEFILE:
					handleLargeFile(splitMsg, false);
					break;
				case Constants.ERROR:
					handleExceptionMessage(splitMsg);
					break;
				case Constants.UPDATEPB:
					handleUpdatePb(splitMsg);
					break;
				case Constants.REGISTER:
					handleRegister(splitMsg[1]);
					break;
				case Constants.ALLUSER:
					handleAllUser(splitMsg[1]);
					break;
				case Constants.REQUESTIMAGE:
					handleUpdatePb(splitMsg);
					break;
				case Constants.SAVEFRIENDS:
					if (splitMsg.length == 2) {
						handleFriendList(splitMsg[1]);
					}
					break;
				case Constants.REQUESTCHAT:
					handleRequestChat(splitMsg);
					break;
				case Constants.PREVIEW:
					handlePreview(Integer.parseInt(splitMsg[2]), splitMsg[1], true, MessageState.PARTNER, false);
					break;
				case Constants.FILEID:
					handleFileId(splitMsg);
					break;
				case Constants.WRITING:
					handleWritingState(Boolean.valueOf(splitMsg[1]), splitMsg[2]);
					break;
				case Constants.UPDATESTATUS:
					handleStatusUpdate(splitMsg[1], splitMsg[2]);
				case Constants.REQUESTSTATUS:
					handleStatusUpdate(splitMsg[1], splitMsg[2]);
			}
		}
	}

	private void handleStatusUpdate(String user, String status) {
		status = EmojiParser.parseToUnicode(status);
		chatPartners.getChatPartner(user).setStatus(status);
	}

	private void handlePublicKey(String publicKey) {
		session.sendSymKeyThroughAsym(publicKey);
	}

	private void handleWritingState(Boolean valueOf, String partner) {
		ChatPartner cp = chatPartners.getChatPartner(partner);
		if (cp != null && chatPartners.getPartners().contains(cp)) {
			cp.setWriting(valueOf);
		}

	}

	private void handleFileId(String[] splitMsg) {
		String from = splitMsg[1];
		int id = Integer.parseInt(splitMsg[2]);
		int dbId = Integer.parseInt(splitMsg[3]);
		List<AbstractMessage> messages = chatPartners.getChatPartner(from).getChatMessages();
		new Thread(() -> {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			for (int i = 0; i < messages.size(); i++) {
				AbstractMessage msg = messages.get(i);
				if (msg instanceof LargeFileId && ((LargeFileId) msg).getLargeFileId() != null && ((LargeFileId) msg).getLargeFileId() == id) {
					try {
						FileUtils.copyFile(new File(((PathedMessage) msg).getPath()), new File(FileHandler.getDefaultFilePath() + dbId + "." + FilenameUtils.getExtension(((PathedMessage) msg).getPath())));
					} catch (IOException e) {
						e.printStackTrace();
					}
					return;
				}
			}
		}).start();

	}

	private void handlePreview(int id, String from, boolean unread, MessageState state, boolean requested) {

		runOnMainThread(() -> {
			chatPartners.getChatPartner(from).addMessage(new RequestMessage(id, state), unread, requested);
		});
		runOnMainThread(() -> {

			File fl = FileHandler.hasFile(id + "");
			if (fl != null) {
				presenter.presentFile(from, fl, unread, state, id, requested, null);
			}
		});

	}

	private void handleRequestChat(String[] splitMsg) {
		String partner = splitMsg[2];
		switch (Integer.parseInt(splitMsg[1])) {
			case Constants.TEXT:
				String message = splitMsg[3];
				boolean unread = Boolean.valueOf(splitMsg[4]);
				MessageState state = MessageState.valueOf(splitMsg[5]);
				handleText(partner, message, unread, state, true);
				break;
			case Constants.FILE:
				String base = splitMsg[3];
				String ending = splitMsg[4];
				unread = Boolean.valueOf(splitMsg[5]);
				state = MessageState.valueOf(splitMsg[6]);
				handleSmallFile(base, partner, ending, unread, state, true);
				break;
			case Constants.PREVIEW:
				int id = Integer.parseInt(splitMsg[3]);
				unread = Boolean.valueOf(splitMsg[4]);
				state = MessageState.valueOf(splitMsg[5]);
				handlePreview(id, partner, unread, state, true);
				break;
		}
		Client.getInstance().getController().setRequestSuccessfull();

	}

	private void handleFriendList(String friendList) {
		String friends[] = friendList.split(", ");
		chatPartners.getPartners().clear();
		for (String friend : friends) {
			ChatPartner cp = new ChatPartner(friend, null);
			chatPartners.getPartners().add(cp);
			session.sendImageRequest(cp.getName());
			session.requestChat(cp.getName(), 0);
			session.sendStatusRequest(cp.getName());
			cp.haveMessagesRequested();

		}

	}

	private void handleAllUser(String list) {
		String[] array = list.split(", ");
		Client.getInstance().setall(Arrays.asList(array));

	}

	private void handleRegister(String string) {
		if (string.equalsIgnoreCase("true")) {
			Platform.runLater(() -> {
				Client.getInstance().loginWithRegister();
			});
		} else if (string.equalsIgnoreCase("false")) {
			ExceptionDialog.showDelayedException(Client.getInstance().getLangHandler().get("usernameex"));
			Platform.runLater(() -> {
				Client.getInstance().closeStage();
				Client.getInstance().showRegisterOverview();
			});

		}
	}

	private void handleUpdatePb(String[] splitMsg) {
		byte[] bytes = Base64.getDecoder().decode(splitMsg[2]);
		Image img = new Image(new ByteArrayInputStream(bytes), 50, 50, true, true);
		Platform.runLater(() -> {
			chatPartners.getChatPartner(splitMsg[1]).setImage(img);
		});

	}

	private void handleLogin(String[] splitMsg) {
		byte[] bytes = Base64.getDecoder().decode(splitMsg[2]);
		ByteArrayInputStream is;
		Image img = new Image((is = new ByteArrayInputStream(bytes)), 50, 50, true, true);
		IOUtils.closeQuietly(is);
		runOnMainThread(() -> {
			chatPartners.addChatPartner(splitMsg[1], img);
		});
	}

	private void handleLogout(String logoutName) {
		runOnMainThread(() -> {
			ChatPartner cp = chatPartners.getChatPartner(logoutName);
			if (cp != null && chatPartners.getPartners().contains(cp)) {
				cp.setWriting(false);
			}
			chatPartners.removeChatPartner(logoutName);

		});
	}

	private void handleText(String from, String text, boolean unread, MessageState state, boolean requested) {
		String emojiText = EmojiParser.parseToUnicode(text);
		Matcher m = Constants.HYPERPATTERN.matcher(emojiText);
		String hyperLink = "";
		boolean hasHyper = false;
		if (m.matches()) {
			hasHyper = true;
			hyperLink = m.group(1);
			emojiText = emojiText.replace(hyperLink, "");
		}
		final String doneString = emojiText;
		if (!doneString.isEmpty()) {
			runOnMainThread(() -> {
				chatPartners.getChatPartner(from).addMessage(new TextMessage(doneString, state), unread, requested);
			});
		}
		if (hasHyper) {
			String hyper = hyperLink;
			runOnMainThread(() -> {
				chatPartners.getChatPartner(from).addMessage(new LinkMessage(new Hyperlink(hyper), state), unread, requested);
			});
		}
	}

	public void handleSmallFile(String base, String from, String ending, boolean unread, MessageState state, boolean requested) {
		FileHandler fileHandler = new FileHandler();
		FromFile result = fileHandler.handleFile(base, ending, from);

		presenter.presentFile(result.getFrom(), result.getFile(), unread, state, 0, requested, null);
	}

	public synchronized void handleLargeFile(String[] splitMsg, boolean requested) {
		FileHandler fileHandler;
		int id = Integer.parseInt(splitMsg[6]);
		if (fileHandlers.containsKey(id)) {
			fileHandler = fileHandlers.get(id);

		} else {
			fileHandler = new FileHandler();
			fileHandlers.put(id, fileHandler);
		}
		Optional<FromFile> result = fileHandler.handleMultipleFile(splitMsg);

		Double procent = fileHandler.getProcentual();
		String from = null;
		if (session.getName().equals(splitMsg[1])) {
			from = splitMsg[2];
		} else {
			from = splitMsg[1];
		}
		updateProcessIndicator(procent, from, Integer.parseInt(splitMsg[7]));
		if (result.isPresent()) {
			String ownName = session.getName();
			boolean own = ownName.equals(result.get().getFrom());
			MessageState msgState;
			from = null;
			if (own) {
				msgState = MessageState.ME;
				from = splitMsg[1];
				if (from.equals(ownName)) {
					from = splitMsg[2];
				}
			} else {
				msgState = MessageState.PARTNER;
				from = splitMsg[2];
				if (from.equals(ownName)) {
					from = splitMsg[1];
				}
			}

			Client.getInstance().getController().removeScrollListener();
			presenter.presentFile(from, result.get().getFile(), false, msgState, Integer.parseInt(splitMsg[7]), requested, null);
			fileHandlers.remove(id);
			Client.getInstance().getController().addScrollListener();
		}
	}

	private void updateProcessIndicator(Double procent, String partner, int id) {
		List<AbstractMessage> msgs = chatPartners.getChatPartner(partner).getChatMessages();
		for (int i = 0; i < msgs.size(); i++) {
			AbstractMessage msg = msgs.get(i);
			if (msg instanceof RequestMessage && ((RequestMessage) msg).getId() == id) {
				Platform.runLater(() -> ((RequestMessage) msg).setProcess(procent));

				break;
			}
		}

	}

	private void handleExceptionMessage(String[] splitMsg) {
		ExceptionDialog.showDelayedException(splitMsg[1] + ": " + splitMsg[2]);
	}

	private void runOnMainThread(Runnable runnable) {
		Platform.runLater(runnable);
	}

}
