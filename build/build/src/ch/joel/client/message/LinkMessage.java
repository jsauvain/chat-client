package ch.joel.client.message;

import javafx.scene.control.Hyperlink;

public class LinkMessage extends AbstractMessage{

	private Hyperlink link;
	
	
	public LinkMessage(Hyperlink link, MessageState state) {
		super(state);
		this.link = link;
	}
	
	public Hyperlink getHyperlink() {
		return link;
	}
	
	public void setHyperlink(Hyperlink link) {
		this.link = link;
	}

	
	
	
}
