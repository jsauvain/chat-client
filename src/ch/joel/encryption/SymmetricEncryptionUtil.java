package ch.joel.encryption;
import java.security.Key;
import java.security.SecureRandom;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

public class SymmetricEncryptionUtil {

	private Key key;
	
	public String encrypt(String plaintext) {
		try {
			return encrypt(generateIV(), plaintext);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String encrypt(byte[] iv, String plaintext) throws Exception {
		byte[] decrypted = plaintext.getBytes();
		byte[] encrypted = encrypt(iv, decrypted);

		StringBuilder ciphertext = new StringBuilder();

		ciphertext.append(Base64.getEncoder().encodeToString(iv));
		ciphertext.append(":");
		ciphertext.append(Base64.getEncoder().encodeToString(encrypted));

		return ciphertext.toString();

	}

	public String decrypt(String ciphertext) {
		try {
			String[] parts = ciphertext.split(":");
			byte[] iv = Base64.getDecoder().decode(parts[0]);
			byte[] encrypted = Base64.getDecoder().decode(parts[1]);
			byte[] decrypted = decrypt(iv, encrypted);
			return new String(decrypted);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public SymmetricEncryptionUtil(Key key) {
		this.key = key;
	}

	public SymmetricEncryptionUtil() throws Exception {
		this(generateSymmetricKey());
	}

	public Key getKey() {
		return key;
	}

	public static byte[] generateIV() {
		SecureRandom random = new SecureRandom();
		byte[] iv = new byte[16];
		random.nextBytes(iv);
		return iv;
	}

	public static Key generateSymmetricKey() throws Exception {
		KeyGenerator generator = KeyGenerator.getInstance("AES");
		SecretKey key = generator.generateKey();
		return key;
	}

	public byte[] encrypt(byte[] iv, byte[] plaintext) throws Exception {
		Cipher cipher = Cipher.getInstance(key.getAlgorithm() + "/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
		return cipher.doFinal(plaintext);
	}

	public byte[] decrypt(byte[] iv, byte[] ciphertext) throws Exception {
		Cipher cipher = Cipher.getInstance(key.getAlgorithm() + "/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
		return cipher.doFinal(ciphertext);
	}

}