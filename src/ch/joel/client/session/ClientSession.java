package ch.joel.client.session;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.List;
import java.util.StringJoiner;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import ch.joel.client.Client;
import ch.joel.client.message.MessageState;
import ch.joel.client.message.RequestMessage;
import ch.joel.client.state.ChatPartner;
import ch.joel.client.ui.FilePresenter;
import ch.joel.encryption.AsymmetricEncryptionUtil;
import ch.joel.encryption.SymmetricEncryptionUtil;
import ch.joel.util.Constants;
import ch.joel.util.UserProperties;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;

public class ClientSession {

	private Socket socket;
	private BufferedWriter out;
	private String name;
	private String password;
	private BufferedReader in;
	private AsymmetricEncryptionUtil asymUtil;
	private SymmetricEncryptionUtil symUtil;
	private ClientState state = ClientState.LOGGEDIN;

	public enum ClientState {
		LOGGEDIN, SHUTDOWN;
	}

	public ClientState getState() {
		return state;
	}

	public ClientSession(String username, String host, int port) throws RuntimeException {
		FileHandler.setDefaultPath(host, username);
		this.name = username;
		try {
			socket = new Socket(host, port);
		} catch (UnknownHostException e) {
			throw new RuntimeException(Client.getInstance().getLangHandler().get("hostex"));
		} catch (IOException e) {
			throw new RuntimeException(Client.getInstance().getLangHandler().get("connectex"));
		}
		System.out.println("Connected.");
		try {
			out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e) {
			throw new RuntimeException("Could not open streams");
		}
		asymUtil = new AsymmetricEncryptionUtil();
		try {
			symUtil = new SymmetricEncryptionUtil();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendEncrypted(String message) {
		if (UserProperties.doEncryptions()) {
			send(symUtil.encrypt(message));
		} else {
			send(message);
		}

	}

	private void send(String encryptedMessage) {
		try {
			out.write(encryptedMessage);
			out.newLine();
			out.flush();
		} catch (IOException e) {
			new RuntimeException(e);
		}
	}

	public void login(String password) {
		sendEncrypted(Constants.LOGIN + Constants.SEPERATOR + name + Constants.SEPERATOR + password);
	}

	public void sendNewPaswd(String passwd) {
		sendEncrypted(Constants.CHANGEPASSWORD + Constants.SEPERATOR + passwd);
	}

	public void sendAccountDelete() {
		sendEncrypted(Constants.DELETEACCOUNT + Constants.SEPERATOR);
	}

	public void sendClearUnread(String partner) {
		sendEncrypted(Constants.CLEARUNREAD + Constants.SEPERATOR + partner);
	}

	public void sendFileRequest(int id) {
		sendEncrypted(Constants.REQUESTFILE + Constants.SEPERATOR + id);
	}

	public void sendWritingState(boolean writing, String partner) {
		sendEncrypted(Constants.WRITING + Constants.SEPERATOR + Boolean.toString(writing) + Constants.SEPERATOR + partner);
	}

	public void loginWithRegister() {
		login(this.password);
		password = null;
	}

	public void register(String password) {
		File fl = Client.getProfilePicAsFile();
		this.password = password;
		if (fl != null) {
			try {
				sendEncrypted(Constants.REGISTER + Constants.SEPERATOR + name + Constants.SEPERATOR + password + Constants.SEPERATOR + Base64.getEncoder().encodeToString(Files.readAllBytes(Paths.get(fl.toURI()))));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			BufferedImage buff = SwingFXUtils.fromFXImage(Client.getProfilePic(), null);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try {
				ImageIO.write(buff, "png", baos);
				baos.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			byte[] imageInByte = baos.toByteArray();
			IOUtils.closeQuietly(baos);
			sendEncrypted(Constants.REGISTER + Constants.SEPERATOR + name + Constants.SEPERATOR + password + Constants.SEPERATOR + Base64.getEncoder().encodeToString(imageInByte));
		}

	}

	public void sendImageRequest(String name) {
		sendEncrypted(Constants.REQUESTIMAGE + Constants.SEPERATOR + name);
	}

	public void logout() {
		state = ClientState.SHUTDOWN;
		sendEncrypted(Constants.LOGOUT + Constants.SEPERATOR + name);
	}

	public void sendFile(String path, String reciever, Integer id, RequestMessage rqm) {
		try {
			File file = new File(path);
			if (file.length() > 8192) {
				sendSplitFile(file, reciever, id, rqm);
			} else {
				sendEncrypted(Constants.FILE + Constants.SEPERATOR + reciever + Constants.SEPERATOR + name + Constants.SEPERATOR + Base64.getEncoder().encodeToString(Files.readAllBytes(Paths.get(path))) + Constants.SEPERATOR + FilenameUtils.getExtension(path));
				FilePresenter presenter = new FilePresenter(Client.getInstance().getController().getChatPartners());
				presenter.presentFile(reciever, file, false, MessageState.ME, id, false, null);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private synchronized void sendSplitFile(File file, String receiver, int id, RequestMessage rqm) {
		try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));) {
			byte[] buffer = new byte[8192];
			long packages = file.length() / 8192 + 1;
			if (file.length() % 8192 == 0) {
				packages--;
			}
			for (int i = 0; bis.read(buffer) != -1; i++) {
				String base64 = new String(Base64.getEncoder().encodeToString(buffer));
				StringJoiner joiner = new StringJoiner(Constants.SEPERATOR).add(Constants.LARGEFILE + "").add(receiver).add(name).add(base64).add(FilenameUtils.getExtension(file.getPath())).add(packages + "").add(id + "");
				sendEncrypted(joiner.toString());
				Double proc = getProcentual(i, packages);
				Platform.runLater(() -> rqm.setProcess(proc));
			}
			FilePresenter presenter = new FilePresenter(Client.getInstance().getController().getChatPartners());
			presenter.presentFile(receiver, file, false, MessageState.ME, id, false, id);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private Double getProcentual(int counter, long max) {
		return (double) ((double) counter / (double) max);
	}

	public void setName(String name) {
		this.name = name;
	}

	public void sendPbUpdate(String base64) {
		sendEncrypted(Constants.UPDATEPB + Constants.SEPERATOR + name + Constants.SEPERATOR + base64);
	}

	public void sendMessage(String message, String reciever) {
		sendEncrypted(Constants.TEXT + Constants.SEPERATOR + reciever + Constants.SEPERATOR + name + Constants.SEPERATOR + message);
	}

	public void sendSavedFriends(List<ChatPartner> partners) {
		String finalString = Constants.SAVEFRIENDS + Constants.SEPERATOR;
		for (ChatPartner cp : partners) {
			finalString += cp.getName() + ", ";
		}
		sendEncrypted(finalString);
	}

	public void sendRequestAll() {
		sendEncrypted(Constants.ALLUSER + Constants.SEPERATOR);
	}

	public String read() throws IOException {
		return in.readLine();
	}

	public void close() {
		IOUtils.closeQuietly(out);
		IOUtils.closeQuietly(in);
		IOUtils.closeQuietly(socket);

	}

	public String getName() {
		return name;
	}

	public void requestChat(String name, int starting) {
		sendEncrypted(Constants.REQUESTCHAT + Constants.SEPERATOR + name + Constants.SEPERATOR + starting);

	}

	public void sendSymKeyThroughAsym(String publicKey) {
		send(Constants.SYMKEY + Constants.SEPERATOR + Base64.getEncoder().encodeToString(asymUtil.encrypt(symUtil.getKey().getEncoded(), publicKey)));
	}

	public String decode(String encrypted) {
		return symUtil.decrypt(encrypted);
	}

	public void sendStatusRequest(String user) {
		sendEncrypted(Constants.REQUESTSTATUS + Constants.SEPERATOR + user);

	}

	public void sendStatusUpdate(String status) {
		sendEncrypted(Constants.UPDATESTATUS + Constants.SEPERATOR + status);

	}

}
