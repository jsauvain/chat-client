package ch.joel.util;

import java.util.Timer;
import java.util.TimerTask;

public class WritingTimer extends Timer {

	private static WritingTimer writingRun;

	public void schedule(TimerTask task) {
		writingRun = this;
		super.schedule(task, 5 * 1000);
	}

	public static void disable() {
		if (writingRun != null) {
			writingRun.cancel();
		}
	}

}
