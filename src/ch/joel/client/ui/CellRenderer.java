package ch.joel.client.ui;

import ch.joel.client.Client;
import ch.joel.client.state.ChatPartner;
import ch.joel.client.ui.img.ImageResource;
import ch.joel.client.ui.img.ImageResource.Images;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Callback;

public class CellRenderer implements Callback<ListView<ChatPartner>, ListCell<ChatPartner>> {

	@Override
	public ListCell<ChatPartner> call(ListView<ChatPartner> p) {

		ListCell<ChatPartner> cell = new ListCell<ChatPartner>() {

			@Override
			protected void updateItem(ChatPartner user, boolean bln) {
				super.updateItem(user, bln);
				setGraphic(null);
				setText(null);
				if (user != null) {
					HBox hBox = new HBox();

					Text name = new Text(user.getName());
					name.setFont(Font.font(null, FontWeight.BOLD, 16));

					ImageView statusImageView = new ImageView(new Image(ImageResource.getFilePath((Client.getInstance().getController().getChatPartners().isOnline(user) ? Images.ONLINE : Images.OFFLINE))));
					statusImageView.setFitHeight(16);
					statusImageView.setFitWidth(16);

					ImageView pictureImageView = new ImageView();
					pictureImageView.setImage(user.getImage());
					HBox.setMargin(pictureImageView, new Insets(10));

					Label unreadMessages;
					if (user.getUnread() != 0) {
						unreadMessages = new Label(user.getUnread() + "");
						unreadMessages.getStyleClass().add("unread");
					} else {
						unreadMessages = new Label("");
					}

					Label writingState = new Label();
					writingState.textProperty().bind(user.getWriting());
					writingState.setMaxWidth(200);
					
					VBox textVBox = new VBox(name, writingState);
					textVBox.setPadding(new Insets(10, 0, 0, 0));

					VBox vBox = new VBox();
					vBox.getChildren().add(unreadMessages);
					vBox.setAlignment(Pos.CENTER_RIGHT);

					Region rg = new Region();
					HBox.setHgrow(rg, Priority.ALWAYS);
					hBox.getChildren().addAll(statusImageView, pictureImageView, textVBox, rg, vBox);
					hBox.setAlignment(Pos.CENTER_LEFT);
					setGraphic(hBox);
					setOnMouseClicked((event) -> {
						if (event.getButton() == MouseButton.SECONDARY) {
							MenuItem item = new MenuItem(Client.getInstance().getLangHandler().get("remove"));
							item.setOnAction(new EventHandler<ActionEvent>() {

								@Override
								public void handle(ActionEvent event) {
									Client.getInstance().getController().getChatPartners().removeFromGUI(user);

								}
							});
							ContextMenu menu = new ContextMenu(item);
							setContextMenu(menu);

						}
					});
				}
			}
		};
		return cell;
	}
}
