package ch.joel.client.updater;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class Updater {

	private static final int VERSION = 5;

	public void update() {

		try {
			URL url = new URL("http://joelsauv.myhostpoint.ch/chat/latest/Version.txt");
			InputStream is = url.openStream();
			byte[] bytes = new byte[10];
			is.read(bytes);
			is.close();
			String string = new String(bytes).trim();
			int serverVersion = Integer.parseInt(string.split(" ")[1]);
			if (VERSION < serverVersion) {
				url = new URL("http://joelsauv.myhostpoint.ch/chat/latest/Installer.exe");
				is = url.openStream();
				byte[] buffer = new byte[8192];
				File fl = new File(System.getProperty("user.home") + "/Downloads/Installer.exe");
				FileOutputStream fos = new FileOutputStream(fl);
				int amount;
				while ((amount = is.read(buffer)) != -1) {
					fos.write(buffer, 0, amount);
				}
				is.close();
				fos.close();
				Desktop.getDesktop().open(fl);
				System.exit(0);
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}
}
