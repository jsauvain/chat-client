package ch.joel.client.message;

public interface PathedMessage {

	public String getPath();
}
