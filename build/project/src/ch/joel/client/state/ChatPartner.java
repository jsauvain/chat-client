package ch.joel.client.state;

import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;

import com.vdurmont.emoji.EmojiManager;

import ch.joel.client.Client;
import ch.joel.client.message.AbstractMessage;
import ch.joel.client.message.FileMessage;
import ch.joel.client.message.ImageMessage;
import ch.joel.client.message.LinkMessage;
import ch.joel.client.message.MessageState;
import ch.joel.client.message.RequestMessage;
import ch.joel.client.message.TextMessage;
import ch.joel.client.message.VideoMessage;
import ch.joel.client.ui.controller.ExtendedTray;
import ch.joel.languages.LanguageHandler;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.image.Image;
import javafx.scene.paint.Paint;
import javafx.util.Duration;
import tray.animations.AnimationType;

public class ChatPartner {

	private String name;
	private Image profilePicture;
	private List<AbstractMessage> messages;
	private int unreadMsg = 0;
	private SimpleStringProperty writing;
	private boolean haveMessagesRequested = false;
	private String status;

	public ChatPartner(String name, Image profilePic) {
		this.name = name;
		this.profilePicture = profilePic;
		messages = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public Image getImage() {
		return profilePicture;
	}

	public void setImage(Image img) {
		this.profilePicture = img;
		String activeChat = Client.getInstance().getController().getChatPartnerName();
		if (name.equals(activeChat)) {
			Client.getInstance().getController().setChatPartnerImage(profilePicture);
		}
		Client.getInstance().getController().updateList();
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public synchronized void addMessage(AbstractMessage message) {
		addMessage(message, true, false);

	}

	public synchronized void addMessage(AbstractMessage message, boolean unread, boolean requested) {
		String activeChat = Client.getInstance().getController().getChatPartnerName();
		if (requested) {
			messages.add(0, message);
		} else {
			messages.add(message);
		}
		setWriting(false);
		if (unread) {
			Client.getInstance().getController().addToGUI(this);
			ChatPartner selected = Client.getInstance().getController().getSelectedUser();
			Client.getInstance().getController().removeSelectionListener();
			Client.getInstance().getController().setSelectedUser(null);
			Client.getInstance().setChatPartnerOnTop(this);
			Client.getInstance().getController().setSelectedUser(selected);
			Client.getInstance().getController().addSelectionListener();
		}
		if (name.equals(activeChat)) {
			Client.getInstance().getController().updateChat(message, requested);

			if (Client.isMinimized() && unread && !requested) {
				sendNotification(message);
			}
		} else {
			if (unread) {
				unreadMsg++;
				Client.getInstance().getController().updateList();
				if (!requested) {
					sendNotification(message);
				}
			}
		}
	}

	public void setMessage(AbstractMessage message, int index) {
		String activeChat = Client.getInstance().getController().getChatPartnerName();
		messages.set(index, message);
		if (name.equals(activeChat)) {
			Client.getInstance().getController().updateChatReplacement(message, index, false);
		}
	}

	public void incrementUnread() {
		unreadMsg++;
		Client.getInstance().getController().updateList();
	}

	public void setUnread(int num) {
		unreadMsg = num;
		Client.getInstance().getController().updateList();
	}

	private void sendNotification(AbstractMessage message) {
		ExtendedTray tray = new ExtendedTray();
		Client.getInstance().toFront();
		tray.setTitle(name);
		tray.setAnimationType(AnimationType.POPUP);
		tray.setRectangleFill(Paint.valueOf("#ffaa00"));
		tray.setImage(new Image("http://www.iconsdb.com/icons/preview/orange/chat-4-xxl.png"));
		tray.setOnClick(event -> {
			Platform.runLater(() -> {
				Client.getInstance().getController().setSelectedUser(this);
				Client.getInstance().setIconified(false);
				tray.dismiss();
			});
		});
		tray.setMessage(getMessageFromEachType(message));
		Toolkit.getDefaultToolkit().beep();
		tray.showAndDismiss(Duration.seconds(5));

	}

	private String getMessageFromEachType(AbstractMessage message) {
		LanguageHandler lang = Client.getInstance().getLangHandler();
		if (message instanceof TextMessage) {
			return ((TextMessage) message).getMessage();
		} else if (message instanceof ImageMessage) {
			return EmojiManager.getForAlias("camera").getUnicode() + " " + lang.get("image");
		} else if (message instanceof VideoMessage) {
			return EmojiManager.getForAlias("movie_camera").getUnicode() + " " + lang.get("media");
		} else if (message instanceof LinkMessage) {
			return ((LinkMessage) message).getHyperlink().getText();
		} else if (message instanceof FileMessage) {
			return EmojiManager.getForAlias("file_folder").getUnicode() + " " + lang.get("file");
		} else if (message instanceof RequestMessage) {
			return EmojiManager.getForAlias("file_folder").getUnicode() + " " + lang.get("file");
		}
		return null;
	}

	public List<AbstractMessage> getChatMessages() {
		return messages;
	}

	public void clearUnread() {
		unreadMsg = 0;
		Client.getInstance().getController().updateList();

	}

	public int getUnread() {
		return unreadMsg;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ChatPartner) {
			if (((ChatPartner) obj).name.equals(name)) {
				return true;
			}
		}
		return false;
	}

	public void haveMessagesRequested() {
		haveMessagesRequested = true;
	}

	public boolean doHaveMessagesRequested() {
		return haveMessagesRequested;
	}

	public void setWriting(boolean val) {
		if (writing == null) {
			getWriting();
		}

		if (val) {
			Platform.runLater(() -> writing.set(Client.getInstance().getLangHandler().get("writing") + "."));
		} else {
			Platform.runLater(() -> writing.set(getLatestText()));
		}
	}

	public void setWritingStyle(String writing) {
		Platform.runLater(() -> this.writing.set(writing));
	}

	public SimpleStringProperty getWriting() {
		if (writing == null) {
			return (writing = new SimpleStringProperty(getLatestText()));
		} else {
			return writing;
		}
	}

	public boolean getWritingState() {
		if (writing != null && writing.equals(getLatestText())) {
			return false;
		}
		return true;
	}

	public String getLatestText() {
		if (messages.size() >= 1) {
			AbstractMessage msg = messages.get(messages.size() - 1);
			return (msg.getMessageState() == MessageState.ME ? "Du: " : "") + getMessageFromEachType(msg);
		}
		return "";
	}

}
