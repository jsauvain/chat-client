package ch.joel.client.message;

public abstract class AbstractMessage implements Message {

	private MessageState msgState;

	public AbstractMessage(MessageState state) {
		this.msgState = state;

	}

	@Override
	public MessageState getMessageState() {
		return msgState;
	}

}
