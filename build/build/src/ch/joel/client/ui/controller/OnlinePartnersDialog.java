package ch.joel.client.ui.controller;

import java.io.IOException;

import ch.joel.client.Client;
import ch.joel.client.state.ChatPartner;
import ch.joel.client.state.ChatPartners;
import ch.joel.client.ui.view.FXMLResource;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;

public class OnlinePartnersDialog {

	private ChatPartners cp;
	private Stage stage;
	private AnchorPane root;
	@FXML
	private ListView<ChatPartner> listView;
	@FXML
	private ScrollPane scroll;
	@FXML
	private Label label;
	@FXML
	private Button other;

	public OnlinePartnersDialog(ChatPartners cps) {
		this.cp = cps;
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(FXMLResource.load(FXMLResource.FXML.OnlinePartners));
		loader.setController(this);
		try {
			root = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene(root);
		stage = new Stage();
		stage.setScene(scene);
		stage.setMinHeight(500);
		stage.setMinWidth(300);
		stage.setAlwaysOnTop(true);
		label.setText(Client.getInstance().getLangHandler().get("choosetoadd"));
		other.setText(Client.getInstance().getLangHandler().get("other"));
		stage.getIcons().add(new Image("http://www.iconsdb.com/icons/preview/orange/chat-4-xxl.png"));
		listView.setItems(cps.getnotAddedPartners());
		listView.setFixedCellSize(70);
		setListView();
		stage.show();

	}

	@FXML
	private void showOther() {
		new AllChaterDialog(cp);
		stage.close();
	}

	private void setListView() {
		listView.setCellFactory(new Callback<ListView<ChatPartner>, ListCell<ChatPartner>>() {

			@Override
			public ListCell<ChatPartner> call(ListView<ChatPartner> param) {

				ListCell<ChatPartner> cell = new ListCell<ChatPartner>() {

					@Override
					protected void updateItem(ChatPartner user, boolean bln) {
						super.updateItem(user, bln);
						setGraphic(null);
						setText(null);
						if (user != null) {
							HBox hBox = new HBox();

							Text name = new Text(user.getName());

							ImageView pictureImageView = new ImageView();
							pictureImageView.setImage(user.getImage());
							HBox.setMargin(pictureImageView, new Insets(10));
							hBox.getChildren().addAll(pictureImageView, name);
							hBox.setAlignment(Pos.CENTER_LEFT);
							setGraphic(hBox);
							setOnMouseClicked((event) -> {
								if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
									Platform.runLater(() -> {
										cp.addToGUI(user);
										stage.close();
									});
								}
							});
						}
					}

				};
				return cell;
			}
		});
	}
}
