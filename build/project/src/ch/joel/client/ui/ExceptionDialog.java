package ch.joel.client.ui;

import java.util.Timer;
import java.util.TimerTask;

import ch.joel.client.Client;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class ExceptionDialog {

	public static void showException(String message) {
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Dialog");
				alert.setHeaderText(Client.getInstance().getLangHandler().get("attention"));
				alert.setContentText(message);
				new Timer().schedule(new TimerTask() {

					@Override
					public void run() {
						Platform.runLater(() -> {
							alert.close();
						});

					}
				}, 5000);

				alert.showAndWait();

			}
		});

	}

	public static void showDelayedException(String message) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				showException(message);

			}
		}).start();
	}
}
