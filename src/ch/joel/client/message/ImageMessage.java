package ch.joel.client.message;

import javafx.scene.image.Image;

public class ImageMessage extends AbstractMessage implements LargeFileId, PathedMessage {

	private Image image;
	private String path;
	private Integer largeFileId;

	public ImageMessage(Image image, String path, MessageState state, Integer largeFileId) {
		super(state);
		this.path = path;
		this.image = image;
		this.largeFileId = largeFileId;

	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	@Override
	public String getPath() {
		return path;
	}

	@Override
	public Integer getLargeFileId() {
		return largeFileId;
	}

}
