package ch.joel.client.ui;

import java.io.File;
import java.net.MalformedURLException;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import ch.joel.client.message.AbstractMessage;
import ch.joel.client.message.FileMessage;
import ch.joel.client.message.ImageMessage;
import ch.joel.client.message.MessageState;
import ch.joel.client.message.RequestMessage;
import ch.joel.client.message.VideoMessage;
import ch.joel.client.state.ChatPartners;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.media.Media;

public class FilePresenter {

	private ChatPartners cps;

	public FilePresenter(ChatPartners cps) {
		this.cps = cps;

	}

	public void presentFile(String from, File fl, boolean unread, MessageState state, int fileId, boolean requested, Integer largeFileId) {

		switch (FilenameUtils.getExtension(fl.getPath()).toLowerCase()) {
			case "jpg":
			case "png":
			case "gif":
				showImage(from, fl, unread, state, fileId, requested, largeFileId);
				break;
			case "mp4":
			case "m4a":
			case "wav":
				showVideo(from, fl, unread, state, fileId, requested, largeFileId);
				break;
			default:
				showDefaultImage(from, fl, unread, state, fileId, requested, largeFileId);

		}

	}

	private void showDefaultImage(String from, File fl, boolean unread, MessageState state, int fileId, boolean requested, Integer largeFileId) {
		Platform.runLater((new Runnable() {

			@Override
			public void run() {
				try {

					if (fileId == 0) {
						cps.getChatPartner(from).addMessage(new FileMessage(fl, state, largeFileId), unread, requested);
					} else {
						List<AbstractMessage> msgs = cps.getChatPartner(from).getChatMessages();
						int finalNumber = 0;
						for (int i = 0; i < msgs.size(); i++) {
							AbstractMessage msg = msgs.get(i);
							if (msg instanceof RequestMessage && ((RequestMessage) msg).getId() == fileId) {
								finalNumber = i;
								break;
							}
						}
						cps.getChatPartner(from).setMessage(new FileMessage(fl, state, largeFileId), finalNumber);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}));

	}

	private void showImage(String from, File fl, boolean unread, MessageState state, int fileId, boolean requested, Integer largeFileId) {
		Platform.runLater((new Runnable() {

			@Override
			public void run() {
				try {
					if (fileId == 0) {
						cps.getChatPartner(from).addMessage(new ImageMessage(new Image(fl.toURI().toURL().toString(), 100, 100, true, true), fl.getPath(), state, largeFileId), unread, requested);
					} else {
						List<AbstractMessage> msgs = cps.getChatPartner(from).getChatMessages();
						int finalNumber = 0;
						for (int i = 0; i < msgs.size(); i++) {
							AbstractMessage msg = msgs.get(i);
							if (msg instanceof RequestMessage && ((RequestMessage) msg).getId() == fileId) {
								finalNumber = i;
								break;
							}
						}
						cps.getChatPartner(from).setMessage(new ImageMessage(new Image(fl.toURI().toURL().toString(), 100, 100, true, true), fl.getPath(), state, largeFileId), finalNumber);
					}
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}

			}
		}));
	}

	private void showVideo(String from, File fl, boolean unread, MessageState state, int fileId, boolean requested, Integer largeFileId) {

		Platform.runLater((new Runnable() {

			@Override
			public void run() {
				try {

					if (fileId == 0) {
						cps.getChatPartner(from).addMessage(new VideoMessage(new Media(fl.toURI().toURL().toString()), fl.getPath(), state, largeFileId), unread, requested);
					} else {
						List<AbstractMessage> msgs = cps.getChatPartner(from).getChatMessages();
						int finalNumber = 0;
						for (int i = 0; i < msgs.size(); i++) {
							AbstractMessage msg = msgs.get(i);
							if (msg instanceof RequestMessage && ((RequestMessage) msg).getId() == fileId) {
								finalNumber = i;
								break;
							}
						}
						cps.getChatPartner(from).setMessage(new VideoMessage(new Media(fl.toURI().toURL().toString()), fl.getPath(), state, largeFileId), finalNumber);
					}
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}

			}
		}));
	}

}
