package ch.joel.client.session;

import java.io.File;

public class FromFile {

	private String from;
	private File fl;

	public FromFile(String from, File fl) {
		this.from = from;
		this.fl = fl;

	}
	
	public String getFrom() {
		return from;
	}
	
	public File getFile() {
		return fl;
	}
}
