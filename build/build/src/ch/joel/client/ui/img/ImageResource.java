package ch.joel.client.ui.img;

public class ImageResource {

	public static String getFilePath(Images name) {
		return ImageResource.class.getResource(name.toString()).toString();
	}
	
	public enum Images {

		FILE("file.png"), LOGIN("login.jpg"), PROFILE("profile.png"), DOWNLOAD("download.png"), MEDIA("media.png"), ONLINE("online.png"), OFFLINE("offline.png"), FONT("OpenSansEmoji.ttf");

		private Images(final String file) {
			this.file = file;
		}

		private final String file;

		@Override
		public String toString() {
			return file;

		}

	}
}