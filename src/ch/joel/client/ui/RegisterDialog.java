package ch.joel.client.ui;

import java.util.Optional;

import ch.joel.client.Client;
import ch.joel.client.ui.img.ImageResource;
import ch.joel.client.ui.img.ImageResource.Images;
import ch.joel.languages.LanguageHandler;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

public class RegisterDialog {

	private boolean username = false;
	private boolean password = false;
	private boolean server = false;
	private boolean port = false;

	public boolean okButton = false;

	public String[] loginDialog() {
		// Create the custom dialog.
		okButton = false;
		LanguageHandler lang = Client.getInstance().getLangHandler();
		Dialog<Pair<String, String>> dialog = new Dialog<>();
		dialog.setTitle(lang.get("registering"));
		dialog.setHeaderText(lang.get("registertext"));

		// Set the icon (must be included in the project).
		dialog.setGraphic(new ImageView(ImageResource.getFilePath(Images.LOGIN)));

		// Set the button types.
		ButtonType registerButtonType = new ButtonType(lang.get("register"), ButtonData.OK_DONE);
		ButtonType cancel = new ButtonType(lang.get("cancel"), ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().addAll(registerButtonType, cancel);

		// Create the username and password labels and fields.
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField username = new TextField();
		username.setPromptText(lang.get("username"));
		PasswordField password = new PasswordField();
		password.setPromptText(lang.get("password"));
		TextField server = new TextField();
		server.setPromptText(lang.get("host"));
		TextField port = new TextField();
		port.setPromptText(lang.get("port"));

		grid.add(new Label(lang.get("username") + ":"), 0, 0);
		grid.add(username, 1, 0);
		grid.add(new Label(lang.get("password") + ":"), 0, 1);
		grid.add(password, 1, 1);
		grid.add(new Label(lang.get("host") + ":"), 0, 2);
		grid.add(server, 1, 2);
		grid.add(new Label(lang.get("port") + ":"), 0, 3);
		grid.add(port, 1, 3);

		// Enable/Disable login button depending on whether a username was
		// entered.

		Node loginButton = dialog.getDialogPane().lookupButton(registerButtonType);
		loginButton.setDisable(true);

		// Do some validation (using the Java 8 lambda syntax).
		username.textProperty().addListener((observable, oldValue, newValue) -> {
			setUsername(!newValue.trim().isEmpty());
			tryActivate(loginButton);

		});

		password.textProperty().addListener((observable, oldValue, newValue) -> {
			setPassword(!newValue.trim().isEmpty());
			tryActivate(loginButton);
		});

		server.textProperty().addListener((observable, oldValue, newValue) -> {
			setHost(!newValue.trim().isEmpty());
			tryActivate(loginButton);
		});

		port.textProperty().addListener((observable, oldValue, newValue) -> {
			setPort(!newValue.trim().isEmpty());
			tryActivate(loginButton);
		});

		dialog.getDialogPane().setContent(grid);

		// Request focus on the username field by default.
		Platform.runLater(() -> username.requestFocus());

		// Convert the result to a username-password-pair when the login button
		// is clicked.
		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == registerButtonType) {
				okButton = true;
				return new Pair<>(username.getText(), server.getText());
			}
			return null;
		});

		Optional<Pair<String, String>> result = dialog.showAndWait();

		String[] userServer = new String[4];
		result.ifPresent(usernameServer -> {
			userServer[0] = usernameServer.getKey();
			userServer[1] = password.getText();
			userServer[2] = usernameServer.getValue();
			userServer[3] = port.getText();
		});
		if (okButton) {
			return userServer;
		} else {
			return null;
		}
	}

	private void tryActivate(Node loginButton) {
		if (this.username && this.password && this.server && this.port) {
			loginButton.setDisable(false);
		} else {
			loginButton.setDisable(true);
		}
	}

	private void setUsername(boolean bool) {
		username = bool;
	}

	private void setPassword(boolean bool) {
		password = bool;
	}

	private void setHost(boolean bool) {
		server = bool;
	}

	private void setPort(boolean bool) {
		port = bool;
	}

}
