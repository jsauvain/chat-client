package ch.joel.client;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

import ch.joel.client.session.ClientSession;
import ch.joel.client.session.FileHandler;
import ch.joel.client.session.MessageProcessor;
import ch.joel.client.session.ReadingThread;
import ch.joel.client.state.ChatPartner;
import ch.joel.client.state.ChatPartners;
import ch.joel.client.ui.ExceptionDialog;
import ch.joel.client.ui.LoginDialog;
import ch.joel.client.ui.RegisterDialog;
import ch.joel.client.ui.controller.ChatOverviewController;
import ch.joel.client.ui.controller.RootLayoutController;
import ch.joel.client.ui.img.ImageResource;
import ch.joel.client.ui.img.ImageResource.Images;
import ch.joel.client.updater.Updater;
import ch.joel.languages.LanguageHandler;
import ch.joel.util.NumberUtil;
import ch.joel.util.UserProperties;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class Client extends Application {

	private Stage applicationStage;
	private BorderPane rootLayout;
	private ChatOverviewController controller;
	private ClientSession session;
	private static boolean isMinimized = false;
	private ChatPartners cp;
	private List<String> allCp;
	private LanguageHandler langHandler;

	private static Client client;

	public static void main(String[] args) {
		new Updater().update();
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Font.loadFont(ImageResource.getFilePath(Images.FONT), 10);
		client = this;
		cp = new ChatPartners();
		this.applicationStage = primaryStage;
		this.applicationStage.setTitle("Chat Client " + (char) 8482);
		this.applicationStage.getIcons().add(new Image("http://www.iconsdb.com/icons/preview/orange/chat-4-xxl.png"));
		this.applicationStage.setMinWidth(1000);
		this.applicationStage.setMinHeight(400);
		this.applicationStage.iconifiedProperty().addListener((listener, bool, bool2) -> {
			isMinimized = bool2.booleanValue();
		});
		new UserProperties().loadProperties();
		langHandler = new LanguageHandler();
		showLoginOverview();
	}

	public static Client getInstance() {
		return client;
	}

	@Override
	public void stop() throws Exception {
		saveFriends();
		try {
			session.logout();
			session.close();
			FileUtils.deleteDirectory(new File(FileHandler.getDefaultFilePath() + "Temp/"));
			System.exit(0);
		} catch (NullPointerException e) {
		}
	}

	private void saveFriends() {
		if (session != null) {
			session.sendSavedFriends(cp.getPartners());
		}
	}

	public void initRootLayout() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Client.class.getResource("ui/view/RootLayout.fxml"));
			loader.setController(new RootLayoutController());
			rootLayout = (BorderPane) loader.load();

			// Show the scene containing the root layout.
			Scene scene = new Scene(rootLayout);
			applicationStage.setScene(scene);

			applicationStage.show();
		} catch (IOException e) {
			ExceptionDialog.showException(e.getMessage());
		}
	}

	public void showLoginOverview() {
		try {

			LoginDialog dialog = new LoginDialog();
			String datas[] = null;
			boolean wasKeeped = false;
			if (Boolean.valueOf(UserProperties.getKeepLogged())) {
				datas = dialog.loginDialog(UserProperties.getUser(), UserProperties.getPass(), UserProperties.getHost(), UserProperties.getPort(), true);
				wasKeeped = true;
			} else {
				datas = dialog.loginDialog();
			}
			if (datas != null && datas[0] != null && datas[1] != null && datas[2] != null && !datas[0].equals("") && !datas[1].equals("") && !datas[2].equals("") && NumberUtil.isNumber(datas[3])) {

				if (Boolean.valueOf(datas[4])) {
					UserProperties.setLogin("true", datas[0], datas[1], datas[2], datas[3]);
					UserProperties.saveValues();
				} else {
					if (wasKeeped) {
						UserProperties.setKeepLogged("false");
						UserProperties.clear();
						UserProperties.saveValues();
					}
				}
				try {
					session = new ClientSession(datas[0], datas[2], Integer.parseInt(datas[3]));
					File fl = new File(FileHandler.getDefaultPath());
					fl.mkdirs();
					new ReadingThread(session, new MessageProcessor(cp, session)).start();
					initRootLayout();
					login(datas[1]);
				} catch (RuntimeException e) {
					ExceptionDialog.showException(e.getMessage());
					showLoginOverview();
				}
			} else {
				if (dialog.okButton) {
					ExceptionDialog.showException(langHandler.get("fillfields"));
					showLoginOverview();
				} else if (!dialog.toRegistration) {
					System.exit(0);
				}
			}
		} catch (RuntimeException e) {
			ExceptionDialog.showException(e.getMessage());
			e.printStackTrace();
		}
	}

	public void login(String password) {
		session.login(password);
		showChatOverview(session, cp);
		requestAll();
	}

	public void loginWithRegister() {
		session.loginWithRegister();
		showChatOverview(session, cp);
		requestAll();
	}

	private void requestAll() {
		session.sendRequestAll();
	}

	public void showRegisterOverview() {
		try {

			RegisterDialog dialog = new RegisterDialog();
			String datas[] = dialog.loginDialog();
			if (datas != null && NumberUtil.isNumber(datas[3])) {
				if (checkPassword(datas[1])) {
					if (checkUser(datas[0])) {
						try {
							session = new ClientSession(datas[0], datas[2], Integer.parseInt(datas[3]));
							File fl = new File(FileHandler.getDefaultPath());
							fl.mkdirs();
							new ReadingThread(session, new MessageProcessor(cp, session)).start();
							initRootLayout();
							session.register(datas[1]);
						} catch (RuntimeException e) {
							e.printStackTrace();
							ExceptionDialog.showException(e.getMessage());
							showRegisterOverview();
						}
					} else {
						ExceptionDialog.showDelayedException(langHandler.get("usernameinvalid"));
						showRegisterOverview();
					}
				} else {
					ExceptionDialog.showDelayedException(langHandler.get("passwordinvalid"));
					showRegisterOverview();
				}
			} else {
				if (dialog.okButton) {
					ExceptionDialog.showDelayedException(langHandler.get("fillfields"));
					showRegisterOverview();
				} else {
					showLoginOverview();
				}
			}
		} catch (RuntimeException e) {
			ExceptionDialog.showException(e.getMessage());
		}
	}

	private boolean checkUser(String string) {
		if (string.length() > 16 || string.matches(".*\\W+.*")) {
			return false;
		}
		return true;
	}

	public boolean checkPassword(String string) {
		if (string.length() < 6) {
			return false;
		}
		if (string.toLowerCase().equals(string) || string.toUpperCase().equals(string)) {
			return false;
		}
		if (!string.matches(".*\\d+.*")) {
			return false;
		}
		if (!string.matches(".*\\W+.*")) {
			return false;
		}
		return true;

	}

	public static Image getProfilePic() {
		String extension = "png";
		if (!hasPicture(extension)) {
			extension = "jpg";
			if (!hasPicture(extension)) {
				extension = "gif";
				if (!hasPicture(extension)) {
					return new Image(ImageResource.getFilePath(Images.PROFILE), 50, 50, true, true);
				}
			}
		}
		try {
			return new Image(new File(FileHandler.getDefaultPath() + "profile." + extension).toURI().toURL().toString(), 50, 50, true, true);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static File getProfilePicAsFile() {
		String extension = "png";
		if (!hasPicture(extension)) {
			extension = "jpg";
			if (!hasPicture(extension)) {
				extension = "gif";
				if (!hasPicture(extension)) {
					return null;
				}
			}
		}
		return new File(FileHandler.getDefaultPath() + "profile." + extension);
	}

	private static boolean hasPicture(String extension) {
		File picture = new File(FileHandler.getDefaultPath() + "profile." + extension);
		return picture.exists();

	}

	public void showChatOverview(ClientSession session, ChatPartners cp) {
		try {
			// Load chat overview.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Client.class.getResource("ui/view/ChatOverview.fxml"));
			controller = new ChatOverviewController();
			loader.setController(controller);
			AnchorPane chatOverview = (AnchorPane) loader.load();
			// Set chat overview into the center of root layout.
			rootLayout.setCenter(chatOverview);

			// Give the controller access to the stage.
			controller.init(session, cp);
		} catch (IOException e) {
			e.printStackTrace();
			ExceptionDialog.showException(e.getMessage());
		}
	}

	public ChatOverviewController getController() {
		return controller;
	}

	public void setIconified(boolean booln) {
		applicationStage.setIconified(booln);
	}

	public void toFront() {
		applicationStage.toFront();
	}

	public void closeStage() {
		applicationStage.close();

	}

	public static boolean isMinimized() {
		return isMinimized;
	}

	public File openDialog(FileChooser fileChooser) {
		return fileChooser.showOpenDialog(getStage());
	}

	public synchronized void setChatPartnerOnTop(ChatPartner cp) {
		this.cp.setOnTop(cp);
	}

	public void setall(List<String> list) {
		allCp = new ArrayList<>(list);
		allCp.remove(session.getName());
	}

	public List<String> getAll() {
		return allCp;
	}

	public ClientSession getSession() {
		return session;
	}

	public LanguageHandler getLangHandler() {
		return langHandler;
	}

	public Stage getStage() {
		return applicationStage;
	}
}
