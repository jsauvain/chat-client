package ch.joel.client.session;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;

public class FileHandler {

	private String path;
	private File fl;
	private boolean first = true;
	private int counter = 0;
	private int amount = 0;
	private OutputStream stream;

	private static String defaultPath;

	public FileHandler() {
		File file = new File(getDefaultFilePath() + "Temp/");
		file.mkdirs();
	}

	public FromFile handleFile(String base, String ending, String from) {
		byte[] imgBytes = Base64.getDecoder().decode(base);
		createFile(ending);

		try (OutputStream stream = new FileOutputStream(fl.getPath())) {
			stream.write(imgBytes);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		return new FromFile(from, fl);
	}

	public Optional<FromFile> handleMultipleFile(String[] splitMsg) {
		byte[] imgBytes = Base64.getDecoder().decode(splitMsg[3]);
		if (first) {
			first = false;
			createLargeFile(Integer.parseInt(splitMsg[7]), splitMsg[4]);
			counter = 0;
			try {
				stream = new FileOutputStream(fl.getPath(), true);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		try {
			stream.write(imgBytes);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		if (++counter == (amount = Integer.parseInt(splitMsg[5]))) {
			try {
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return Optional.of(new FromFile(splitMsg[2], fl));
		}
		return Optional.empty();
	}

	private void createFile(String ending) {
		this.path = getDefaultFilePath() + "Temp/" + new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date()) + "." + ending;
		fl = new File(this.path);
		fl.deleteOnExit();
	}

	private void createLargeFile(int dbId, String ending) {
		this.path = getDefaultFilePath() + dbId + "." + ending;
		fl = new File(this.path);
	}

	public static File hasFile(String fileNameNoExt) {
		File fl = new File(getDefaultFilePath());
		File[] files = fl.listFiles((fileDir, name) -> {
			String fileName = name.split("\\.")[0];
			if (fileName.equals(fileNameNoExt)) {
				return true;
			}
			return false;
		});
		if (files != null && files.length >= 1) {
			return files[0];
		}
		return null;
	}

	public Double getProcentual() {
		return (double) ((double) counter / (double) amount);
	}

	public static String getDefaultFilePath() {
		return defaultPath + "Files/";
	}

	public static String getDefaultPath() {
		return defaultPath;
	}

	public static void setDefaultPath(String host, String username) {
		defaultPath = System.getProperty("user.home") + "/AppData/Local/ChatClient/" + host + "/" + username + "/";
	}

}
