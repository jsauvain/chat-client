package ch.joel.client.message;

public class TextMessage extends AbstractMessage{

	private String message;
	
	public TextMessage(String message, MessageState state) {
		super(state);
		this.message = message;
		
	}
	
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	
}
