package ch.joel.client.message;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class VideoMessage extends AbstractMessage implements LargeFileId, PathedMessage {

	private Media media;
	private String path;
	private MediaPlayer player;
	private Integer largeFileId;

	public VideoMessage(Media video, String path, MessageState state, Integer largeFileId) {
		super(state);
		this.path = path;
		this.media = video;
		this.player = new MediaPlayer(media);
		this.largeFileId = largeFileId;
	}

	public Media getVideo() {
		return media;
	}

	public void setVideo(Media video) {
		this.media = video;
	}

	public MediaPlayer getMediaPlayer() {
		return player;
	}

	@Override
	public String getPath() {
		return path;
	}

	@Override
	public Integer getLargeFileId() {
		return largeFileId;
	}

}
