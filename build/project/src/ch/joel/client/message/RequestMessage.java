package ch.joel.client.message;

import javafx.beans.property.SimpleDoubleProperty;

public class RequestMessage extends AbstractMessage {

	private int id;
	private SimpleDoubleProperty process = new SimpleDoubleProperty(-1);

	public RequestMessage(int id, MessageState state) {
		super(state);
		this.id = id;

	}

	public int getId() {
		return id;
	}

	public SimpleDoubleProperty getProcess() {
		return process;
	}

	public void setProcess(double num) {
		process.set(num);
	}

}
