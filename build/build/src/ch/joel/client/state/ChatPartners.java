package ch.joel.client.state;

import java.util.ArrayList;
import java.util.List;

import ch.joel.client.Client;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;

public class ChatPartners {

	private ObservableList<ChatPartner> partners;

	private List<ChatPartner> onlineChater;

	private ObservableList<ChatPartner> notAddedCps;

	public ChatPartners() {
		onlineChater = new ArrayList<>();
		partners = FXCollections.observableArrayList();
		notAddedCps = FXCollections.observableArrayList();
	}

	public ChatPartner getChatPartner(String name) {
		for (ChatPartner partner : partners) {
			if (partner.getName().equals(name)) {
				return partner;
			}
		}
		for (ChatPartner partner : onlineChater) {
			if (partner.getName().equals(name)) {
				return partner;
			}
		}
		return null;
	}

	public void addToGUI(ChatPartner cp) {
		partners.add(0, cp);
		if (!cp.doHaveMessagesRequested()) {
			Client.getInstance().getSession().requestChat(cp.getName(), cp.getChatMessages().size());
			cp.haveMessagesRequested();
		}
	}

	public void addChatPartner(String name, Image img) {
		if (partners.contains(new ChatPartner(name, null))) {
			for (ChatPartner partner : partners) {
				if (partner.getName().equals(name)) {
					onlineChater.add(0, partner);
					Client.getInstance().getController().updateList();
					return;
				}
			}

		}
		onlineChater.add(0, new ChatPartner(name, img));
	}

	public void removeChatPartner(String name) {
		ChatPartner partner = getChatPartner(name);
		onlineChater.remove(partner);
		Client.getInstance().getController().updateList();
	}

	public void removeFromGUI(ChatPartner name) {
		if (name.getName().equals(Client.getInstance().getController().getChatPartnerName())) {
			Client.getInstance().getController().showChat(null);
			Client.getInstance().getController().clearChatPartnerName();
		}
		partners.remove(name);
	}

	public ObservableList<ChatPartner> getPartners() {
		return partners;
	}

	public ObservableList<ChatPartner> getnotAddedPartners() {
		notAddedCps.clear();
		notAddedCps.addAll(onlineChater);
		notAddedCps.removeAll(partners);
		return notAddedCps;
	}

	public void setOnTop(ChatPartner cp) {
		if (partners.contains(cp)) {
			partners.remove(cp);
			partners.add(0, cp);
		}
	}

	public List<ChatPartner> getOnlinePartners() {
		List<ChatPartner> result = new ArrayList<>(onlineChater);
		result.retainAll(partners);
		return result;
	}

	public boolean isOnline(ChatPartner cp) {
		return onlineChater.contains(cp);
	}
}
