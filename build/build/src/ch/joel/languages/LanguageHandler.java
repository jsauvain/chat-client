package ch.joel.languages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

import ch.joel.util.UserProperties;

public class LanguageHandler {

	private String path;
	private Properties prop;
	private String language;
	private boolean loaded = false;

	public LanguageHandler() {
		File fl = new File(System.getProperty("user.home") + "/AppData/Local/ChatClient/Language/");
		fl.mkdirs();
		path = System.getProperty("user.home") + "/AppData/Local/ChatClient/Language/";
		prop = new Properties();
		language = UserProperties.getLanguage();

	}

	private void loadDefaultFiles() {
		File de = new File(path + DefaultLanguages.GERMAN.toString());
		if (!de.exists()) {
			try {
				IOUtils.copy(getClass().getResourceAsStream(DefaultLanguages.GERMAN.toString()), new FileOutputStream(de));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		File en = new File(path + DefaultLanguages.ENGLISH.toString());
		if (!en.exists()) {
			try {
				IOUtils.copy(getClass().getResourceAsStream(DefaultLanguages.ENGLISH.toString()), new FileOutputStream(en));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	private void load() {
		loadDefaultFiles();
		File fl = new File(path + language + ".properties");
		if (!fl.exists()) {
			fl = new File(path + "de.properties");
		}
		try (InputStream is = new FileInputStream(fl)) {
			prop.load(is);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String get(String property) {
		if (!loaded) {
			load();
			loaded = true;
		}
		return prop.getProperty(property);
	}

	public enum DefaultLanguages {

		GERMAN("de.properties"), ENGLISH("en.properties");

		private String path;

		DefaultLanguages(String path) {
			this.path = path;
		}

		@Override
		public String toString() {
			return path;

		}

	}

}
