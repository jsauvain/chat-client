package ch.joel.client.ui.view;

import java.net.URL;

public class FXMLResource {

	public static URL load(FXML fxml) {

		return FXMLResource.class.getResource(fxml.toString());
	}

	public enum FXML {

		OnlinePartners("OnlinePartners.fxml"), AllChater("AllChater.fxml"), Tray("TrayNotification.fxml"), Informations("Informations.fxml"), Settings("Settings.fxml"), Emoji("AppleColorEmoji.ttf");

		private FXML(String fxml) {
			this.fxml = fxml;
		}

		private final String fxml;

		@Override
		public String toString() {
			return fxml;

		}

	}
}
